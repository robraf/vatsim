package r.vatsim.view;

public class Stats {
    private int atcSize;
    private int pilotSize;
    private int airportSize;
    private int totalClient;
    private String lastUpdate;

    public Stats() {
    }

    public int getAtcSize() {
        return atcSize;
    }

    public void setAtcSize(int atcSize) {
        this.atcSize = atcSize;
    }

    public int getPilotSize() {
        return pilotSize;
    }

    public void setPilotSize(int pilotSize) {
        this.pilotSize = pilotSize;
    }

    public int getAirportSize() {
        return airportSize;
    }

    public void setAirportSize(int airportSize) {
        this.airportSize = airportSize;
    }

    public int getTotalClient() {
        return totalClient;
    }

    public void setTotalClient(int totalClient) {
        this.totalClient = totalClient;
    }

    public String getLastUpdate() {
        return lastUpdate;
    }

    public void setLastUpdate(String lastUpdate) {
        this.lastUpdate = lastUpdate;
    }
}
