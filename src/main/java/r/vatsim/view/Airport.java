package r.vatsim.view;

import lombok.Data;

import java.util.ArrayList;
import java.util.List;

@Data
public class Airport {
    private String icao;
    private String iata;
    private String city;
    private String country;
    private String name;

    private double latitude;
    private double longitude;
    private int elevation;

    private boolean display = false;
    private boolean isAtc = false;

    private List<Atc> atcList = new ArrayList<>();
    private List<Pilot> arrivalList = new ArrayList<>();
    private List<Pilot> departureList = new ArrayList<>();

    public Airport(String icao){
        this.icao = icao;
    }

    public String getIcao() {
        return icao;
    }

    public void setIcao(String icao) {
        this.icao = icao;
    }

    public String getIata() {
        return iata;
    }

    public void setIata(String iata) {
        this.iata = iata;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public double getLatitude() {
        return latitude;
    }

    public void setLatitude(double latitude) {
        this.latitude = latitude;
    }

    public double getLongitude() {
        return longitude;
    }

    public void setLongitude(double longitude) {
        this.longitude = longitude;
    }

    public int getElevation() {
        return elevation;
    }

    public void setElevation(int elevation) {
        this.elevation = elevation;
    }

    public List<Atc> getAtcList() {
        return atcList;
    }

    public void setAtcList(List<Atc> atcList) {
        this.atcList = atcList;
    }

    public boolean isDisplay() {
        return display;
    }

    public void setDisplay(boolean display) {
        this.display = display;
    }

    public List<Pilot> getArrivalList() {
        return arrivalList;
    }

    public void setArrivalList(List<Pilot> arrivalList) {
        this.arrivalList = arrivalList;
    }

    public List<Pilot> getDepartureList() {
        return departureList;
    }

    public void setDepartureList(List<Pilot> departureList) {
        this.departureList = departureList;
    }

    public boolean isAtc() {
        return isAtc;
    }

    public void setAtc(boolean atc) {
        isAtc = atc;
    }
}

