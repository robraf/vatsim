package r.vatsim.view;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Data;

import javax.persistence.criteria.CriteriaBuilder;
import java.time.Instant;
import java.time.temporal.ChronoField;

@Data
public class Atc {
    private String callsign;
    private String cid;
    private String atis;
    private String frequency;
    private String type;
    private String airport;
    private String rating;
    private int offline_count = 0;
    private Instant login_time;
    private Instant last_time;

    private double latitude;
    private double longitude;

    private long logon_time=0;

    @JsonIgnore
    private boolean newAtc = false;

    @JsonIgnore
    private Atc oldAtc;

    public Atc(String callsign, String cid, String atis, String frequency, String type, String airport, String rating) {
        this.callsign = callsign;
        this.cid = cid;
        this.atis = atis;
        this.frequency = frequency;
        this.type = type;
        this.airport = airport;
        this.rating = rating;
        this.login_time = Instant.now();
        this.last_time = Instant.now();
    }

    public void updateFields(Atc updatedAtc, Instant date) {
        if(!this.cid.equals(updatedAtc.cid)){
            oldAtc = new Atc(this.getCallsign(), this.getCid(), this.getAtis(), this.getFrequency(), this.getType(), this.getAirport(), this.getRating());
            this.newAtc = true;
            this.logon_time=0;
            this.login_time = Instant.now();
        }
        else{
            oldAtc = null;
            this.newAtc = false;
        }

        this.callsign = updatedAtc.callsign;
        this.cid = updatedAtc.cid;
        this.atis = updatedAtc.atis;
        this.frequency = updatedAtc.frequency;
        this.type = updatedAtc.type;
        this.airport = updatedAtc.airport;
        this.rating = updatedAtc.rating;
        this.offline_count = updatedAtc.offline_count;
        this.last_time = date;
        this.logon_time=Instant.now().getLong(ChronoField.INSTANT_SECONDS) - this.login_time.getLong(ChronoField.INSTANT_SECONDS);
    }

    public String getCallsign() {
        return callsign;
    }

    public void setCallsign(String callsign) {
        this.callsign = callsign;
    }

    public String getCid() {
        return cid;
    }

    public void setCid(String cid) {
        this.cid = cid;
    }

    public String getAtis() {
        return atis;
    }

    public void setAtis(String atis) {
        this.atis = atis;
    }

    public String getFrequency() {
        return frequency;
    }

    public void setFrequency(String frequency) {
        this.frequency = frequency;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getAirport() {
        return airport;
    }

    public void setAirport(String airport) {
        this.airport = airport;
    }

    public String getRating() {
        return rating;
    }

    public void setRating(String rating) {
        this.rating = rating;
    }

    public int getOffline_count() {
        return offline_count;
    }

    public void setOffline_count(int offline_count) {
        this.offline_count = offline_count;
    }

    public Instant getLogin_time() {
        return login_time;
    }

    public void setLogin_time(Instant login_time) {
        this.login_time = login_time;
    }

    public double getLatitude() {
        return latitude;
    }

    public void setLatitude(double latitude) {
        this.latitude = latitude;
    }

    public double getLongitude() {
        return longitude;
    }

    public void setLongitude(double longitude) {
        this.longitude = longitude;
    }

    public boolean isNewAtc() {
        return newAtc;
    }

    public void setNewAtc(boolean newAtc) {
        this.newAtc = newAtc;
    }

    public Instant getLast_time() {
        return last_time;
    }

    public void setLast_time(Instant last_time) {
        this.last_time = last_time;
    }

    public Atc getOldAtc() {
        return oldAtc;
    }

    public void setOldAtc(Atc oldAtc) {
        this.oldAtc = oldAtc;
    }

    public long getLogon_time() {
        return logon_time;
    }

    public void setLogon_time(long logon_time) {
        this.logon_time = logon_time;
    }
}
