package r.vatsim.view;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Data;

import java.time.Instant;
import java.util.ArrayList;
import java.util.List;

@Data
public class Pilot {
    private String callsign;
    private String cid;

    private String aircraft;
    private String transponder;
    private String airport_departure;
    private String airport_arrival;
    private String airport_alternate;
    private String rules;
    private String planned_altitiude;

    private String route;
    private String remarks;

    private double latitude;
    private double longitude;
    private int altitude;
    private int speed;
    private int heading;

    private double distanceToDestination=-1.0;
    private double distanceTotal=-1.0;
    private double flightTime = -1.0;
    private String flightStatus = "";
    private boolean inAir = true;
    private double estimatedTime = -1.0;
    private String airline = "";
    private String cityArrival = "";
    private String cityDeparture = "";
    private Instant startFlight = null;

    @JsonIgnore
    private boolean newPilot = false;

    @JsonIgnore
    private Pilot oldPilot;

    private int offline_count = 0;

    @JsonIgnore
    private List<Waypoint> waypointArrayList = new ArrayList<>();

    public Pilot(String callsign, String cid, String aircraft, String transponder, String airport_departure, String airport_arrival, String airport_alternate, String route, String remarks, String rules, double latitude, double longitude, int altitude, int speed, int heading, String planned_altitiude) {
        this.callsign = callsign;
        this.cid = cid;
        this.aircraft = aircraft;
        this.transponder = transponder;
        this.airport_departure = airport_departure;
        this.airport_arrival = airport_arrival;
        this.airport_alternate = airport_alternate;
        this.route = route;
        this.remarks = remarks;
        this.latitude = latitude;
        this.longitude = longitude;
        this.altitude = altitude;
        this.speed = speed;
        this.rules = rules;
        this.heading = heading;
        this.planned_altitiude = planned_altitiude;
        if(speed>0 && this.startFlight==null){
            this.startFlight = Instant.now();
        }
    }

    public String getCallsign() {
        return callsign;
    }

    public void setCallsign(String callsign) {
        this.callsign = callsign;
    }

    public String getCid() {
        return cid;
    }

    public void setCid(String cid) {
        this.cid = cid;
    }

    public String getAircraft() {
        return aircraft;
    }

    public void setAircraft(String aircraft) {
        this.aircraft = aircraft;
    }

    public String getTransponder() {
        return transponder;
    }

    public void setTransponder(String transponder) {
        this.transponder = transponder;
    }

    public String getAirport_departure() {
        return airport_departure;
    }

    public void setAirport_departure(String airport_departure) {
        this.airport_departure = airport_departure;
    }

    public String getAirport_arrival() {
        return airport_arrival;
    }

    public void setAirport_arrival(String airport_arrival) {
        this.airport_arrival = airport_arrival;
    }

    public String getAirport_alternate() {
        return airport_alternate;
    }

    public void setAirport_alternate(String airport_alternate) {
        this.airport_alternate = airport_alternate;
    }

    public String getRoute() {
        return route;
    }

    public void setRoute(String route) {
        this.route = route;
    }

    public String getRemarks() {
        return remarks;
    }

    public void setRemarks(String remarks) {
        this.remarks = remarks;
    }

    public double getLatitude() {
        return latitude;
    }

    public void setLatitude(double latitude) {
        this.latitude = latitude;
    }

    public double getLongitude() {
        return longitude;
    }

    public void setLongitude(double longitude) {
        this.longitude = longitude;
    }

    public int getAltitude() {
        return altitude;
    }

    public void setAltitude(int altitude) {
        this.altitude = altitude;
    }

    public int getSpeed() {
        return speed;
    }

    public void setSpeed(int speed) {
        this.speed = speed;
    }

    public String getRules() {
        return rules;
    }

    public void setRules(String rules) {
        this.rules = rules;
    }

    public int getOffline_count() {
        return offline_count;
    }

    public void setOffline_count(int offline_count) {
        this.offline_count = offline_count;
    }

    public List<Waypoint> getWaypointArrayList() {
        return waypointArrayList;
    }

    public void setWaypointArrayList(List<Waypoint> waypointArrayList) {
        this.waypointArrayList = waypointArrayList;
    }

    public void addWaypoint(Waypoint waypoint)
    {
        this.waypointArrayList.add(waypoint);
    }

    public boolean isNewPilot() {
        return newPilot;
    }

    public void setNewPilot(boolean newPilot) {
        this.newPilot = newPilot;
    }

    public Pilot getOldPilot() {
        return oldPilot;
    }

    public void setOldPilot(Pilot oldPilot) {
        this.oldPilot = oldPilot;
    }

    public String getAirline() {
        return airline;
    }

    public void setAirline(String airline) {
        this.airline = airline;
    }

    public int getHeading() {
        return heading;
    }

    public void setHeading(int heading) {
        this.heading = heading;
    }

    public double getDistanceToDestination() {
        return distanceToDestination;
    }

    public void setDistanceToDestination(double distanceToDestination) {
        this.distanceToDestination = distanceToDestination;
    }

    public String getFlightStatus() {
        return flightStatus;
    }

    public void setFlightStatus(String flightStatus) {
        this.flightStatus = flightStatus;
    }

    public double getEstimatedTime() {
        return estimatedTime;
    }

    public void setEstimatedTime(double estimatedTime) {
        this.estimatedTime = estimatedTime;
    }

    public boolean isInAir() {
        return inAir;
    }

    public void setInAir(boolean inAir) {
        this.inAir = inAir;
    }

    public String getCityArrival() {
        return cityArrival;
    }

    public void setCityArrival(String cityArrival) {
        this.cityArrival = cityArrival;
    }

    public String getCityDeparture() {
        return cityDeparture;
    }

    public void setCityDeparture(String cityDeparture) {
        this.cityDeparture = cityDeparture;
    }

    public double getDistanceTotal() {
        return distanceTotal;
    }

    public void setDistanceTotal(double distanceTotal) {
        this.distanceTotal = distanceTotal;
    }

    public Instant getStartFlight() {
        return startFlight;
    }

    public void setStartFlight(Instant startFlight) {
        this.startFlight = startFlight;
    }

    public double getFlightTime() {
        return flightTime;
    }

    public void setFlightTime(double flightTime) {
        this.flightTime = flightTime;
    }

    public String getPlanned_altitiude() {
        return planned_altitiude;
    }

    public void setPlanned_altitiude(String planned_altitiude) {
        this.planned_altitiude = planned_altitiude;
    }

    public void updateFields(Pilot updatedPilot, Instant date) {
        //jesli zmieni callsign to zapisz do historii
        if(!this.cid.equals(updatedPilot.cid)){
            oldPilot = new Pilot(this.getCallsign(), this.getCid(), this.getAircraft(), this.transponder, this.airport_departure, this.airport_arrival, this.airport_alternate, this.route, this.remarks, this.rules,this.getLatitude(), this.getLongitude(), this.getAltitude(), this.getSpeed(), this.heading, this.planned_altitiude);
            for (Waypoint waypoint : waypointArrayList) {
                oldPilot.waypointArrayList.add(waypoint);
            }

            this.waypointArrayList.clear();
            this.waypointArrayList.add(new Waypoint(updatedPilot.latitude, updatedPilot.longitude, updatedPilot.speed, updatedPilot.altitude, date, updatedPilot.heading));
            this.newPilot = true;
        }else{
            oldPilot = null;
            this.newPilot = false;
        }

        if(this.startFlight==null && updatedPilot.speed>0){
            this.startFlight = date;
        }

        this.cid = updatedPilot.cid;
        this.callsign = updatedPilot.callsign;
        this.aircraft = updatedPilot.aircraft;
        this.transponder = updatedPilot.transponder;
        this.airport_departure = updatedPilot.airport_departure;
        this.airport_arrival = updatedPilot.airport_arrival;
        this.airport_alternate = updatedPilot.airport_alternate;
        this.route = updatedPilot.route;
        this.remarks = updatedPilot.remarks;
        this.rules = updatedPilot.rules;
        this.offline_count = updatedPilot.offline_count;
        this.latitude = updatedPilot.latitude;
        this.longitude = updatedPilot.longitude;
        this.altitude = updatedPilot.altitude;
        this.speed = updatedPilot.speed;
        this.heading = updatedPilot.heading;
        this.planned_altitiude = updatedPilot.planned_altitiude;
    }
}
