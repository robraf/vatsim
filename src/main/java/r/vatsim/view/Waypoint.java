package r.vatsim.view;

import lombok.Data;

import java.time.Instant;

@Data
public class Waypoint {
    private double latitude;
    private double longitude;
    private int speed;
    private int altitude;
    private int heading;
    private Instant date;

    public Waypoint(double latitude, double longitude, int speed, int altitude, Instant date, int heading) {
        this.latitude = latitude;
        this.longitude = longitude;
        this.speed = speed;
        this.altitude = altitude;
        this.heading = heading;
        this.date = date; //przy każdym ticku serwera ustalić date, aby wszystkie waypointy mialy tą samą (potrzebne w pryzszlosci w playbackach)
    }

    public double getLatitude() {
        return latitude;
    }

    public void setLatitude(double latitude) {
        this.latitude = latitude;
    }

    public double getLongitude() {
        return longitude;
    }

    public void setLongitude(double longitude) {
        this.longitude = longitude;
    }

    public int getSpeed() {
        return speed;
    }

    public void setSpeed(int speed) {
        this.speed = speed;
    }

    public int getAltitude() {
        return altitude;
    }

    public int getHeading() {
        return heading;
    }

    public void setHeading(int heading) {
        this.heading = heading;
    }

    public void setAltitude(int altitude) {
        this.altitude = altitude;
    }

    public Instant getDate() {
        return date;
    }

    public void setDate(Instant date) {
        this.date = date;
    }
}
