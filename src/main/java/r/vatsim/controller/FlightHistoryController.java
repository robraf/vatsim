package r.vatsim.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import r.vatsim.config.KeyAPI;
import r.vatsim.model.FlightHistory;
import r.vatsim.service.FlightHistoryService;
import r.vatsim.service.WaypointHistoryService;

import java.util.List;

@RestController
@RequestMapping("/api/history/flight")
@CrossOrigin
public class FlightHistoryController {
    @Autowired
    FlightHistoryService flightHistoryService;

    @Autowired
    WaypointHistoryService waypointHistoryService;

    @GetMapping(value="/callsign", produces = "application/json")
    public List<FlightHistory> getHistoryByCallsign(@RequestParam("name") String callsign, @RequestParam("key") String key){
        if(new KeyAPI().API_KEY.equals(key)) {
            return flightHistoryService.findByCallsign(callsign);
        }
        return null;
    }

    @GetMapping(value="/id", produces = "application/json")
    public FlightHistory getHistoryById(@RequestParam("name") long id, @RequestParam("key") String key){
        if(new KeyAPI().API_KEY.equals(key)) {
            return flightHistoryService.findById(id);
        }
        return null;
    }

}
