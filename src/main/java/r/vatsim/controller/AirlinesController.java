package r.vatsim.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import r.vatsim.config.KeyAPI;
import r.vatsim.model.Airlines;
import r.vatsim.service.AirlinesService;

@RestController
@RequestMapping("/api/airlines")
@CrossOrigin
public class AirlinesController {
    @Autowired
    AirlinesService airlinesService;

    @PostMapping(produces = "application/json")
    public Airlines addAirlines(@RequestBody Airlines airlines, @RequestParam("key") String key){
        if(new KeyAPI().API_KEY.equals(key)) {
           Airlines airlinesObj = airlinesService.add(airlines);
            if (airlinesObj != null)
                return airlinesObj;
            else
                return new Airlines();
        }
        return null;
    }

    @GetMapping(value="/icao", produces = "application/json")
    public Airlines getAirlinesByIcao(@RequestParam("name") String icao, @RequestParam("key") String key){
        if(new KeyAPI().API_KEY.equals(key)) {
            return airlinesService.findByICAO(icao);
        }
        return null;
    }
}
