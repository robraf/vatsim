package r.vatsim.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import r.vatsim.config.KeyAPI;
import r.vatsim.model.AtcHistory;
import r.vatsim.model.FlightHistory;
import r.vatsim.service.AtcHistoryService;

import java.util.List;

@RestController
@RequestMapping("/api/history/atc")
@CrossOrigin
public class AtcHistoryController {
    @Autowired
    AtcHistoryService atcHistoryService;

    @GetMapping(value="/callsign", produces = "application/json")
    public List<AtcHistory> getHistoryByCallsign(@RequestParam("name") String callsign, @RequestParam("key") String key){
        if(new KeyAPI().API_KEY.equals(key)) {
            return atcHistoryService.findByCallsign(callsign);
        }
        return null;
    }

    @GetMapping(value="/cid", produces = "application/json")
    public List<AtcHistory> getHistoryByCid(@RequestParam("name") String cid, @RequestParam("key") String key){
        if(new KeyAPI().API_KEY.equals(key)) {
            return atcHistoryService.findByCid(cid);
        }
        return null;
    }

    @GetMapping(value="/airport", produces = "application/json")
    public List<AtcHistory> getHistoryByAirport(@RequestParam("name") String airport, @RequestParam("key") String key){
        if(new KeyAPI().API_KEY.equals(key)) {
            return atcHistoryService.findByAirport(airport);
        }
        return null;
    }

    @GetMapping( produces = "application/json")
    public List<AtcHistory> getHistoryAll(@RequestParam("key") String key){
        if(new KeyAPI().API_KEY.equals(key)) {
            return atcHistoryService.findAll();
        }
        return null;
    }
}
