package r.vatsim.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import r.vatsim.config.KeyAPI;
import r.vatsim.session.SessionService;
import r.vatsim.view.*;

import java.util.List;

@RestController
@RequestMapping("/api/session")
@CrossOrigin
public class SessionController {
    @Autowired
    SessionService sessionService;

    @GetMapping(value="/pilots", produces = "application/json")
    public List<Pilot> getPilotList(@RequestParam("key") String key){
        if(new KeyAPI().API_KEY.equals(key))
        {
            return sessionService.getListOfPilots();
        }
        return null;
    }

    @GetMapping(value="/atc", produces = "application/json")
    public List<Atc> getAtcList(@RequestParam("key") String key){
        if(new KeyAPI().API_KEY.equals(key))
        {
            return sessionService.getListOfAtc();
        }
        return null;
    }

    @GetMapping(value="/airport", produces = "application/json")
    public List<Airport> getAirportList(@RequestParam("key") String key){
        if(new KeyAPI().API_KEY.equals(key))
        {
            return sessionService.getListOfAirport();
        }
        return null;
    }

    @GetMapping(value="/airport/icao", produces = "application/json")
    public Airport getAirportByIcao(@RequestParam("name") String icao, @RequestParam("key") String key){
        if(new KeyAPI().API_KEY.equals(key)) {
            return sessionService.getByIcao(icao);
        }
        return null;
    }

    @GetMapping(value="/pilots/callsign", produces = "application/json")
    public Pilot getPilotByCallsign(@RequestParam("name") String callsign, @RequestParam("key") String key){
        if(new KeyAPI().API_KEY.equals(key)) {
            return sessionService.getByCallsign(callsign);
        }
        return null;
    }

    @GetMapping(value="/pilots/waypoint/callsign", produces = "application/json")
    public List<Waypoint> getWaypointByCallsign(@RequestParam("name") String callsign, @RequestParam("key") String key){
        if(new KeyAPI().API_KEY.equals(key)) {
            return sessionService.getWaypointOfCallsign(callsign);
        }
        return null;
    }

    @GetMapping(value="/pilots/arrival", produces = "application/json")
    public List<Pilot> getPilotByArrival(@RequestParam("name") String icao, @RequestParam("key") String key){
        if(new KeyAPI().API_KEY.equals(key)) {
            return sessionService.getPilotByArrivalAirport(icao);
        }
        return null;
    }

    @GetMapping(value="/pilots/both", produces = "application/json")
    public List<Pilot> getPilotByBoth(@RequestParam("name") String icao, @RequestParam("key") String key){
        if(new KeyAPI().API_KEY.equals(key)) {
            return sessionService.getPilotByArrivalAndDepartureAirport(icao);
        }
        return null;
    }

    @GetMapping(value="/pilots/departure", produces = "application/json")
    public List<Pilot> getPilotByDeparture(@RequestParam("name") String icao, @RequestParam("key") String key){
        if(new KeyAPI().API_KEY.equals(key)) {
            return sessionService.getPilotByDepartureAirport(icao);
        }
        return null;
    }

    @GetMapping(value="/atc/callsign", produces = "application/json")
    public Atc getAtcByCallsign(@RequestParam("name") String callsign, @RequestParam("key") String key){
        if(new KeyAPI().API_KEY.equals(key)) {
            return sessionService.getAtcByCallsign(callsign);
        }
        return null;
    }

    @GetMapping(value="/atc/airport", produces = "application/json")
    public List<Atc> getAtcByAirport(@RequestParam("name") String airport, @RequestParam("key") String key){
        if(new KeyAPI().API_KEY.equals(key)) {
            return sessionService.getAtcByAirport(airport);
        }
        return null;
    }

    @GetMapping(value="/close", produces = "application/json")
    public void closeSession(@RequestParam("close_key") String close_key, @RequestParam("key") String key){
        if(new KeyAPI().API_KEY.equals(key) && new KeyAPI().KEY_CLOSE_SESSION.equals(close_key)) {
            sessionService.closeSession();
        }
    }

    @GetMapping(value="/stats", produces = "application/json")
    public Stats stats(){
       return sessionService.getStats();
    }

}
