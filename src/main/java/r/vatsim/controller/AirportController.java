package r.vatsim.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import r.vatsim.config.KeyAPI;
import r.vatsim.model.Airport;
import r.vatsim.service.AirportService;

import java.util.List;

@RestController
@RequestMapping("/api/airports")
@CrossOrigin
public class AirportController {
    @Autowired
    AirportService airportService;

    @GetMapping(produces = "application/json")
    public List<Airport> getAirportList(){
        return airportService.findAll();
    }

    @PostMapping(produces = "application/json")
    public Airport addAirport(@RequestBody Airport airport, @RequestParam("key") String key){
        if(new KeyAPI().API_KEY.equals(key)) {
            Airport airportObj = airportService.add(airport);
            if (airportObj != null)
                return airportObj;
            else
                return new Airport();
        }
        return null;
    }

    @GetMapping(value="/icao", produces = "application/json")
    public Airport getAirportByIcao(@RequestParam("name") String icao, @RequestParam("key") String key){
        if(new KeyAPI().API_KEY.equals(key)) {
            return airportService.findByICAO(icao);
        }
        return null;
    }

    @GetMapping(value="/country", produces = "application/json")
    public List<Airport> getAirportsByCountry(@RequestParam("name") String country, @RequestParam("key") String key){
        if(new KeyAPI().API_KEY.equals(key)) {
            return airportService.findByCountry(country);
        }
        return null;
    }
}
