package r.vatsim.session;

import r.vatsim.view.Atc;
import r.vatsim.view.Pilot;
import r.vatsim.view.Waypoint;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.URL;
import java.time.Instant;
import java.time.temporal.ChronoField;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class DataParser {
    final int TIME_NO_ACTIVE = 210;
    final int TIME_NO_ACTIVE_ATC = 120;

    final String data_path = "http://eu.data.vatsim.net/vatsim-data.txt";
    private boolean isClient = false;
    Instant currentDate;
    private String firstLineLast;
    public boolean isUpdate = false;
    /*
    TO-DO
    zabezpieczyc przed errorami 502 itd
     */
    private List<Pilot> listOfPilots = new ArrayList<>();
    private List<Atc> listOfAtc = new ArrayList<>();
    private List<Pilot> listOfPilotsToHistory = new ArrayList<>();
    private List<Atc> listOfAtcToHistory = new ArrayList<>();
    private List<Pilot> actuallyListOfPilots = new ArrayList<>();
    private List<Atc> actuallyListOfAtc = new ArrayList<>();
    private List<Pilot> pilotToDeleteArrayList = new ArrayList<>();
    private List<Atc> atcToDeleteArrayList = new ArrayList<>();

    public DataParser(List<Pilot> listPilot, String oldLine, List<Atc> listAtc) throws IOException {

        URL vatsimURL = new URL(data_path);
        listOfPilots = listPilot;
        listOfAtc = listAtc;
        listOfPilotsToHistory.clear();
        pilotToDeleteArrayList.clear();
        listOfAtcToHistory.clear();
        atcToDeleteArrayList.clear();

        BufferedReader input = new BufferedReader(
                new InputStreamReader(vatsimURL.openStream()));
        String inputLine;


        firstLineLast = inputLine = input.readLine();
        if (!firstLineLast.equals(oldLine)) {
            isUpdate = true;
            currentDate = Instant.now();
            //System.out.println("updated");
            while ((inputLine = input.readLine()) != null) {
                if (inputLine.equals("!CLIENTS:")) {
                    isClient = true;
                }
                if (inputLine.equals(";")) {
                    isClient = false;
                }
                if (isClient && (!inputLine.equals("!CLIENTS:") && !inputLine.equals(";"))) {
                    if (inputLine.contains("\\")) {
                        inputLine = inputLine.replace("\\", "/");
                    }
                    if (inputLine.contains("'")) {
                        inputLine = inputLine.replace("'", " ");
                    }
                    if (inputLine.contains("`")) {
                        inputLine = inputLine.replace("`", " ");
                    }
                    if (inputLine.contains("\"")) {
                        inputLine = inputLine.replace("\"", " ");
                    }
                    String[] arrayClient = inputLine.split(":");
                    String callsign = arrayClient[0];
                    String cid = arrayClient[1];
                    String aircraft = arrayClient[9];
                    String transponder = arrayClient[17];
                    String airport_departure = arrayClient[11];
                    String planned_altitiude = arrayClient[12];
                    String airport_arrival = arrayClient[13];
                    String airport_alternate = arrayClient[28];
                    String route = arrayClient[30];
                    String remarks = arrayClient[29];
                    String clientType = arrayClient[3];
                    String rules = arrayClient[21];

                    double latitude, longitude;
                    int altitude, speed, heading;

                    if (!arrayClient[5].equals("")) {
                        latitude = Double.parseDouble(arrayClient[5]);
                    } else {
                        latitude = 0.0;
                    }

                    if (!arrayClient[6].equals("")) {
                        longitude = Double.parseDouble(arrayClient[6]);
                    } else {
                        longitude = 0.0;
                    }

                    if (!arrayClient[7].equals("")) {
                        altitude = Integer.parseInt(arrayClient[7]);
                    } else {
                        altitude = 0;
                    }

                    if (!arrayClient[8].equals("")) {
                        speed = Integer.parseInt(arrayClient[8]);
                    } else {
                        speed = 0;
                    }

                    if (arrayClient.length != 41) { //ATC
                        if (clientType.equals("ATC")) {
                            Atc atc = null;
                            String atis = arrayClient[35];
                            String frequency = arrayClient[4];
                            String airport = "";
                            String type = "";
                            String rating = arrayClient[16];
                            if (callsign.contains("_")) {
                                String[] splitString = callsign.split("_");
                                if (!splitString[splitString.length - 1].equals("OBS") && !splitString[splitString.length - 1].equals("CTR") && !splitString[splitString.length - 1].equals("SUP")) {
                                    airport = splitString[0];
                                    if (airport.length() == 3) {
                                        airport = "K" + airport;
                                    }
                                }
                                type = splitString[splitString.length - 1];
                                atc = new Atc(callsign, cid, atis, frequency, type, airport, rating);
                            }
                            if (atc != null) {
                                updateExistingAtc(atc);
                                actuallyListOfAtc.add(atc);
                            }
                        }
                    } else { //PILOT
                        if (clientType.equals("PILOT")) {
                            if (!arrayClient[38].equals("")) {
                                heading = Integer.parseInt(arrayClient[38]);
                            } else {
                                heading = 0;
                            }
                            Pilot pilot = new Pilot(callsign, cid, aircraft, transponder, airport_departure, airport_arrival, airport_alternate, route, remarks, rules, latitude, longitude, altitude, speed, heading, planned_altitiude);
                            updateExsistingPilot(pilot);
                            actuallyListOfPilots.add(pilot);
                        }
                    }

                }
            }

            deleteOfflinePilot();
            deleteOfflineAtc();
        }
        input.close();
    }

    private void updateExsistingPilot(Pilot pilot) {
        boolean isExsist = false;
        List<Pilot> result = listOfPilots.stream()
                .filter(item -> item.getCallsign().equals(pilot.getCallsign()))
                .collect(Collectors.toList());

        if (result.size() > 0) {
            isExsist = true;
            Pilot pilotObj = result.get(0);

            pilotObj.updateFields(pilot, currentDate);

            //Na poprzedni callsign wszedł nowy pilot
            if (pilotObj.isNewPilot()) {
                listOfPilotsToHistory.add(pilotObj.getOldPilot());
                //System.out.println("Stary " + pilotObj.getOldPilot());
            } else {
                int countOfWaypoints = pilotObj.getWaypointArrayList().size();
                if (countOfWaypoints > 0) {
                    Waypoint lastWaypoint = pilotObj.getWaypointArrayList().get(countOfWaypoints - 1);
                    if (lastWaypoint.getLatitude() != pilot.getLatitude() || lastWaypoint.getLongitude() != pilot.getLongitude()) {
                        pilotObj.addWaypoint(new Waypoint(pilot.getLatitude(), pilot.getLongitude(), pilot.getSpeed(), pilot.getAltitude(), currentDate, pilot.getHeading()));
                    }
                } else {
                    pilotObj.addWaypoint(new Waypoint(pilot.getLatitude(), pilot.getLongitude(), pilot.getSpeed(), pilot.getAltitude(), currentDate, pilot.getHeading()));
                }
            }
        }

        if (!isExsist) {
            pilot.addWaypoint(new Waypoint(pilot.getLatitude(), pilot.getLongitude(), pilot.getSpeed(), pilot.getAltitude(), currentDate, pilot.getHeading()));
            listOfPilots.add(pilot);
        }
    }

    private void updateExistingAtc(Atc atc) {
        boolean isExist = false;
        List<Atc> result = listOfAtc.stream()
                .filter(item -> item.getCallsign().equals(atc.getCallsign()))
                .collect(Collectors.toList());

        if (result.size() > 0) {
            isExist = true;
            Atc atcObj = result.get(0);
            atcObj.updateFields(atc, currentDate);

            if (atcObj.isNewAtc()) {
                listOfAtcToHistory.add(atcObj.getOldAtc());
                //System.out.println("Stary ATC" + atcObj.getOldAtc());
            }
        }

        if (!isExist) {
            listOfAtc.add(atc);
        }

    }

    private void deleteOfflinePilot() {
        int counter = 0;
        for (Pilot oldPilot : listOfPilots) {
            counter = 0;
            for (Pilot currentPilot : actuallyListOfPilots) {
                if (oldPilot.getCallsign().equals(currentPilot.getCallsign())) {
                    oldPilot.setOffline_count(0);
                    counter++;
                }
            }
            if (counter == 0) {
                oldPilot.setOffline_count(oldPilot.getOffline_count() + 1);
            }

            if (oldPilot.getOffline_count() > 0) {
                int sizeOfWaypointsList = oldPilot.getWaypointArrayList().size();
                long noActiveTime = 0;
                if (sizeOfWaypointsList > 0) {
                    Instant lastLoginTime = oldPilot.getWaypointArrayList().get(sizeOfWaypointsList - 1).getDate();
                    Instant currentTime = Instant.now();
                    noActiveTime = currentTime.getLong(ChronoField.INSTANT_SECONDS) - lastLoginTime.getLong(ChronoField.INSTANT_SECONDS);

                }
                if (noActiveTime > TIME_NO_ACTIVE) {
                    pilotToDeleteArrayList.add(oldPilot);
                }
            }
        }
        for (Pilot pilotToDelete : pilotToDeleteArrayList) {
            listOfPilotsToHistory.add(pilotToDelete);
            listOfPilots.remove(pilotToDelete);
        }
    }

    private void deleteOfflineAtc() {
        int counter = 0;
        for (Atc oldAtc : listOfAtc) {
            counter = 0;
            for (Atc currentAtc : actuallyListOfAtc) {
                if (oldAtc.getCallsign().equals(currentAtc.getCallsign())) {
                    oldAtc.setOffline_count(0);
                    counter++;
                }
            }

            if (counter == 0) {
                oldAtc.setOffline_count(oldAtc.getOffline_count() + 1);
            }

            if (oldAtc.getOffline_count() > 0) {
                long noActiveTime = 0;

                Instant lastLoginTime = oldAtc.getLast_time();
                Instant currentTime = Instant.now();
                noActiveTime = currentTime.getLong(ChronoField.INSTANT_SECONDS) - lastLoginTime.getLong(ChronoField.INSTANT_SECONDS);

                if (noActiveTime > TIME_NO_ACTIVE_ATC) {
                    atcToDeleteArrayList.add(oldAtc);
                }
            }
        }

        for (Atc atcToDelete : atcToDeleteArrayList) {
            listOfAtcToHistory.add(atcToDelete);
            listOfAtc.remove(atcToDelete);
        }

    }

    public List<Pilot> getListOfPilots() {
        return listOfPilots;
    }

    public List<Pilot> getListOfPilotsToHistory() {
        return listOfPilotsToHistory;
    }

    public String getFirstLineLast() {
        return firstLineLast;
    }

    public void setFirstLineLast(String firstLineLast) {
        this.firstLineLast = firstLineLast;
    }

    public List<Atc> getListOfAtc() {
        return listOfAtc;
    }

    public List<Atc> getListOfAtcToHistory() {
        return listOfAtcToHistory;
    }
}
