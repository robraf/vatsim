package r.vatsim.session;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;
import r.vatsim.model.*;
import r.vatsim.service.*;
import r.vatsim.view.Atc;
import r.vatsim.view.Pilot;
import r.vatsim.view.Stats;
import r.vatsim.view.Waypoint;

import java.io.IOException;
import java.sql.Timestamp;
import java.time.Instant;
import java.time.temporal.ChronoField;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

@Service
@Component
public class SessionService {

    private final int refresh_time = 20; //in seconds
    private boolean isSessionClosed = false;
    private boolean canParse = true;
    private DataParser dataParser;
    private String oldLine = "";

    private List<Pilot> listOfPilots = new ArrayList<>();
    private List<Atc> listOfAtc = new ArrayList<>();
    private List<Pilot> listOfFlightToHistory = new ArrayList<>();
    private List<Atc> listOfAtcToHistory = new ArrayList<>();
    private List<r.vatsim.view.Airport> listOfAirport = new ArrayList<>();

    private List<Pilot> tempListOfPilots = new ArrayList<>();
    private List<Atc> tempListOfAtc = new ArrayList<>();
    private List<r.vatsim.view.Airport> tempListOfAirport = new ArrayList<>();

    private Stats stats = new Stats();

    @Autowired
    AirportService airportService;

    @Autowired
    AirlinesService airlinesService;

    @Autowired
    AtcHistoryService atcHistoryService;

    @Autowired
    FlightHistoryService flightHistoryService;

    @Autowired
    WaypointHistoryService waypointHistoryService;

    @Scheduled(fixedRate = 1000 * refresh_time)
    private synchronized void scheduledFunction() throws IOException {
        if (!isSessionClosed && canParse) {
            canParse=false;
            long start = System.currentTimeMillis();
            dataParser = new DataParser(listOfPilots, oldLine, listOfAtc);
            oldLine = dataParser.getFirstLineLast();
            tempListOfPilots = dataParser.getListOfPilots();
            tempListOfAtc = dataParser.getListOfAtc();
            listOfFlightToHistory = dataParser.getListOfPilotsToHistory();
            listOfAtcToHistory = dataParser.getListOfAtcToHistory();
            if (dataParser.isUpdate) {
                setStatusFieldsPilot();
                addPilotToHistory(listOfFlightToHistory);
                setStatusFieldAtc();
                addAtcToHistory(listOfAtcToHistory);
                tempListOfAirport.clear();
                airportListUpdate();
                listOfAirport = tempListOfAirport;
                listOfPilots = tempListOfPilots;
                listOfAtc = tempListOfAtc;
                addToStats();
            }
            //System.out.println("PILOT "+listOfPilots.size()+", ATC: "+listOfAtc.size());
            long end = System.currentTimeMillis();
            System.out.println("TIME " + (end - start) + "ms");
            canParse = true;
        }
    }

    public List<Pilot> getListOfPilots() {
        return listOfPilots;
    }

    public List<Atc> getListOfAtc() {
        return listOfAtc;
    }

    public Pilot getByCallsign(String callsign) {
        List<Pilot> result = listOfPilots.stream()
                .filter(item -> item.getCallsign().equals(callsign))
                .collect(Collectors.toList());

        if (result.size() > 0) {
            return result.get(0);
        }

        return null;
    }

    public List<Pilot> getPilotByArrivalAirport(String icao) {
        List<Pilot> result = listOfPilots.stream()
                .filter(item -> item.getAirport_arrival().equals(icao))
                .collect(Collectors.toList());
        if (result.size() > 0) {
            return result;
        }
        return null;
    }

    public List<Pilot> getPilotByDepartureAirport(String icao) {
        List<Pilot> result = listOfPilots.stream()
                .filter(item -> item.getAirport_departure().equals(icao))
                .collect(Collectors.toList());
        if (result.size() > 0) {
            return result;
        }
        return null;
    }

    public List<Pilot> getPilotByArrivalAndDepartureAirport(String icao) {
        List<Pilot> resultArrival = listOfPilots.stream()
                .filter(item -> item.getAirport_arrival().equals(icao))
                .collect(Collectors.toList());

        List<Pilot> resultDeparture = listOfPilots.stream()
                .filter(item -> item.getAirport_departure().equals(icao))
                .collect(Collectors.toList());

        List<Pilot> result = new ArrayList<>();
        result.addAll(resultArrival);
        result.addAll(resultDeparture);

        if (result.size() > 0) {
            return result;
        }
        return null;
    }

    public r.vatsim.view.Airport getByIcao(String icao) {
        List<r.vatsim.view.Airport> result = listOfAirport.stream()
                .filter(item -> item.getIcao().equals(icao))
                .collect(Collectors.toList());

        if (result.size() > 0) {
            return result.get(0);
        }

        return null;
    }

    public Atc getAtcByCallsign(String callsign) {
        List<Atc> result = listOfAtc.stream()
                .filter(item -> item.getCallsign().equals(callsign))
                .collect(Collectors.toList());
        if (result.size() > 0) {
            return result.get(0);
        }
        return null;
    }

    public List<Waypoint> getWaypointOfCallsign(String callsign) {
        List<Waypoint> waypointList = new ArrayList<>();
        List<Pilot> result = listOfPilots.stream()
                .filter(item -> item.getCallsign().equals(callsign))
                .collect(Collectors.toList());
        if(result.size()>0){
            waypointList=result.get(0).getWaypointArrayList();
        }
        return waypointList;
    }

    public Atc getAtcByType(String type) {
        List<Atc> result = listOfAtc.stream()
                .filter(item -> item.getType().equals(type))
                .collect(Collectors.toList());
        if (result.size() > 0) {
            return result.get(0);
        }
        return null;
    }

    public List<Atc> getAtcByAirport(String airport) {
        List<Atc> result = listOfAtc.stream()
                .filter(item -> item.getAirport().equals(airport))
                .collect(Collectors.toList());

        if (result.size() > 0) {
            return result;
        }

        return null;
    }

    public void addPilotToHistory(List<Pilot> list) {
        for (Pilot pilot : list) {
            FlightHistory flightHistory;
            WaypointHistory waypointHistory;
            if (!pilot.getAirport_arrival().equals("") && !pilot.getAirport_departure().equals("") && !pilot.getAircraft().equals("")) {
                if (pilot.getWaypointArrayList().size() > 2) { //jeśli trasa jest wieksza od 2 punktów
                    flightHistory = new FlightHistory(pilot.getCallsign(), pilot.getCid(), pilot.getAirport_departure(), pilot.getAirport_arrival(), pilot.getAirport_alternate(), pilot.getAircraft(), pilot.getPlanned_altitiude(), pilot.getRules());

                    Waypoint waypointFirstMove = null;
                    for (Waypoint waypoint : pilot.getWaypointArrayList()) {
                        if (waypoint.getSpeed() > 0) {
                            waypointFirstMove = waypoint;
                            break;
                        }
                    }
                    Timestamp dateStart;
                    if (waypointFirstMove != null) {
                        dateStart = Timestamp.from(waypointFirstMove.getDate());
                    } else {
                        dateStart = Timestamp.from(pilot.getWaypointArrayList().get(0).getDate());
                    }
                    Timestamp dateEnd = Timestamp.from(pilot.getWaypointArrayList().get(pilot.getWaypointArrayList().size() - 1).getDate());
                    flightHistory.setDateStart(dateStart);
                    flightHistory.setDateEnd(dateEnd);
                    flightHistoryService.add(flightHistory);
                    for (Waypoint waypoint : pilot.getWaypointArrayList()) {
                        Timestamp date = Timestamp.from(waypoint.getDate());
                        waypointHistory = new WaypointHistory(flightHistory.getId(), waypoint.getLatitude(), waypoint.getLongitude(), waypoint.getSpeed(), waypoint.getAltitude(), date, waypoint.getHeading());
                        waypointHistoryService.add(waypointHistory);
                    }
                    //System.out.println("Dodano do historii pilota " + pilot.getCallsign() + " " + dateStart + " - " + dateEnd);
                }
            }
        }
    }


    private void setStatusFieldsPilot() {
        List<Pilot> pilotsArrivedList = new ArrayList<>();
        Instant timeNow = Instant.now();
        for (Pilot pilot : tempListOfPilots) {
            String destination = "";
            String departure = "";
            String flightStatus = "N/A";
            Airport airportDestination = null;
            Airport airportDeparture = null;
            double distanceFromDepartureAirport = -1.0;
            double distanceFromArrivalAirport = -1.0;
            int heightOverDepartureAirport = -1;
            int heightOverArrivalAirport = -1;
            Airlines airlines = null;
            String icaoAirlines;
            String cityArrival = "";
            String cityDeparture = "";

            //airlines name set
            if (pilot.getAirline().equals("")) {
                if (pilot.getCallsign().length() >= 3) {
                    icaoAirlines = pilot.getCallsign().substring(0, 3);
                    airlines = airlinesService.findByICAO(icaoAirlines);
                    if (airlines != null) {
                        pilot.setAirline(airlines.getName());
                    } else {
                        pilot.setAirline("none");
                    }
                }
                else{
                    pilot.setAirline("none");
                }
            }

            //check airport NONE
            if (pilot.getAirport_departure().equals("NONE")) {
                pilot.setAirport_departure("");
            }

            if (pilot.getAirport_arrival().equals("NONE")) {
                pilot.setAirport_arrival("");
            }

            if (!pilot.getAirport_arrival().equals("")) {
                destination = pilot.getAirport_arrival();
                airportDestination = airportService.findByICAO(destination);
                if (airportDestination != null) {
                    distanceFromArrivalAirport = new Calculate().distance(pilot.getLatitude(), airportDestination.getLatitude(), pilot.getLongitude(), airportDestination.getLongitude(), pilot.getAltitude(), airportDestination.getElevation());
                    pilot.setDistanceToDestination(distanceFromArrivalAirport);
                }
            }

            if (!pilot.getAirport_departure().equals("")) {
                departure = pilot.getAirport_departure();
                airportDeparture = airportService.findByICAO(departure);
                if (airportDeparture != null) {
                    distanceFromDepartureAirport = new Calculate().distance(pilot.getLatitude(), airportDeparture.getLatitude(), pilot.getLongitude(), airportDeparture.getLongitude(), pilot.getAltitude(), airportDeparture.getElevation());
                }
            }

            if (airportDeparture != null) {
                heightOverDepartureAirport = pilot.getAltitude() - airportDeparture.getElevation();
                cityDeparture = airportDeparture.getCity();
            }
            if (airportDestination != null) {
                heightOverArrivalAirport = pilot.getAltitude() - airportDestination.getElevation();
                cityArrival = airportDestination.getCity();
            }

            //set city name
            pilot.setCityArrival(cityArrival);
            pilot.setCityDeparture(cityDeparture);

            //flight status
            int waypointCount = pilot.getWaypointArrayList().size();
            if (waypointCount > 1) {
                int altitiudeDiffrence = pilot.getWaypointArrayList().get(waypointCount - 1).getAltitude() - pilot.getWaypointArrayList().get(waypointCount - 2).getAltitude();
                if (pilot.getSpeed() > 0 && pilot.getSpeed() <= 40 && altitiudeDiffrence <= 100 && altitiudeDiffrence >= -100 && waypointCount > 1) {
                    pilot.setFlightStatus("Taxiing");
                    pilot.setInAir(false);
                } else if (pilot.getSpeed() > 40 && altitiudeDiffrence < -100 && distanceFromArrivalAirport <= 20 && distanceFromArrivalAirport > 8 && airportDestination != null && heightOverArrivalAirport >= 3000 && heightOverArrivalAirport < 10000) {
                    pilot.setFlightStatus("Approaching");
                    pilot.setInAir(true);
                } else if (pilot.getSpeed() > 40 && altitiudeDiffrence <= 100 && altitiudeDiffrence >= -100 && distanceFromArrivalAirport <= 20 && distanceFromArrivalAirport > 8 && airportDestination != null && heightOverArrivalAirport < 3000) {
                    pilot.setFlightStatus("Landing");
                    pilot.setInAir(true);
                } else if (pilot.getSpeed() > 40 && altitiudeDiffrence < -100 && distanceFromArrivalAirport <= 8 && distanceFromArrivalAirport > 0 && airportDestination != null && heightOverArrivalAirport < 3000) {
                    pilot.setFlightStatus("Landing");
                    pilot.setInAir(true);
                } else if (pilot.getSpeed() > 40 && altitiudeDiffrence > 100 && distanceFromDepartureAirport <= 6 && distanceFromDepartureAirport > 0 && airportDeparture != null && heightOverDepartureAirport < 2500) {
                    pilot.setFlightStatus("Taking off");
                    pilot.setInAir(true);
                } else if (pilot.getSpeed() > 40 && altitiudeDiffrence <= 100 && altitiudeDiffrence >= -100 && distanceFromDepartureAirport > 6 && distanceFromArrivalAirport > 8) {
                    pilot.setFlightStatus("Cruising");
                    pilot.setInAir(true);
                } else if (pilot.getSpeed() > 40 && altitiudeDiffrence > 100) {
                    pilot.setFlightStatus("Climbing");
                    pilot.setInAir(true);
                } else if (pilot.getSpeed() > 40 && altitiudeDiffrence < -100) {
                    pilot.setFlightStatus("Descending");
                    pilot.setInAir(true);
                } else if (pilot.getSpeed() == 0 && distanceFromArrivalAirport < 5 && airportDestination != null) {
                    pilot.setFlightStatus("Arrived");
                    pilot.setInAir(false);
                } else if (pilot.getSpeed() == 0 && distanceFromDepartureAirport < 5 && airportDeparture != null && pilot.getWaypointArrayList().size() == 1) {
                    pilot.setFlightStatus("Boarding");
                    pilot.setInAir(false);
                } else {
                    if (pilot.getSpeed() > 60 && altitiudeDiffrence <= 100 && altitiudeDiffrence >= -100) {
                        pilot.setFlightStatus("Cruising");
                        pilot.setInAir(true);
                    } else if (pilot.getSpeed() > 60 && altitiudeDiffrence < -100) {
                        pilot.setFlightStatus("Descending");
                        pilot.setInAir(true);
                    } else if (pilot.getSpeed() > 60 && altitiudeDiffrence > 100) {
                        pilot.setFlightStatus("Climbing");
                        pilot.setInAir(true);
                    }else if(pilot.getSpeed() == 0 && altitiudeDiffrence <= 100 && altitiudeDiffrence >= -100 && waypointCount > 1) {
                        pilot.setFlightStatus("Taxiing");
                        pilot.setInAir(false);
                    }
                    else {
                        pilot.setFlightStatus("N/A");
                        pilot.setInAir(false);
                    }
                }
            } else {
                if (pilot.getSpeed() == 0) {
                    if (distanceFromDepartureAirport < 5 && distanceFromDepartureAirport > 0 && airportDeparture != null) {
                        pilot.setFlightStatus("Boarding");
                        pilot.setInAir(false);
                    } else {
                        pilot.setFlightStatus("N/A");
                        pilot.setInAir(false);
                    }
                } else if (pilot.getSpeed() > 40) {
                    pilot.setFlightStatus("N/A");
                    pilot.setInAir(true);
                } else {
                    pilot.setFlightStatus("N/A");
                    pilot.setInAir(false);
                }
            }

            //time to arrival
            if (distanceFromArrivalAirport >= 0){
                if(pilot.isInAir()){
                    double estimatedTimeInMinutes = distanceFromArrivalAirport / Double.valueOf(pilot.getSpeed());  //t = s/v
                    pilot.setEstimatedTime(estimatedTimeInMinutes); //in hours
                }else{
                    if(distanceFromArrivalAirport<5){
                        pilot.setEstimatedTime(0);
                    }else{
                        pilot.setEstimatedTime(-1);
                    }
                }
            }else{
                pilot.setEstimatedTime(-1);
            }

            //flight time
            if (pilot.getStartFlight() != null) {
                pilot.setFlightTime((timeNow.getLong(ChronoField.INSTANT_SECONDS) - pilot.getStartFlight().getLong(ChronoField.INSTANT_SECONDS)) / 3600.0);
            } else {
                pilot.setFlightTime(-1.0);
            }

            //total distance
            if (airportDeparture != null && airportDestination != null) {
                pilot.setDistanceTotal(new Calculate().distance(airportDeparture.getLatitude(), airportDestination.getLatitude(), airportDeparture.getLongitude(), airportDestination.getLongitude(), airportDeparture.getElevation(), airportDestination.getElevation()));
            }

            //delete if arrived
            if (pilot.getOffline_count() > 0) {
                if (distanceFromArrivalAirport < 5 && pilot.getSpeed() == 0 && pilot.getWaypointArrayList().size() > 2 && airportDestination != null) {
                    pilotsArrivedList.add(pilot);
                    //System.out.println("ARRIVED pilot " + pilot);
                    listOfFlightToHistory.add(pilot);
                }
            }
        }

        //usuwanie arrived pilots
        for (Pilot pilotRemove : pilotsArrivedList) {
            tempListOfPilots.remove(pilotRemove);
        }

    }

    private void setStatusFieldAtc() {
        for (Atc atc : tempListOfAtc) {
            Airport airport;
            if (atc.getAirport() != null) {
                airport = airportService.findByICAO(atc.getAirport());
                if (airport != null) {
                    atc.setLatitude(airport.getLatitude());
                    atc.setLongitude(airport.getLongitude());
                }
            }
        }
    }

    private void addAtcToHistory(List<Atc> list) {
        Instant timeNow = Instant.now();
        for (Atc atc : list) {
            AtcHistory atcHistory;
            if (atc.getLogon_time() > 0 && !atc.getType().equals("SUP") && !atc.getType().equals("OBS") && !atc.getType().equals("ATIS")) {
                atcHistory = new AtcHistory(atc.getCallsign(), atc.getCid(), atc.getAirport(), atc.getLogon_time(), Timestamp.from(atc.getLogin_time()), Timestamp.from(timeNow));
                atcHistoryService.add(atcHistory);
                //System.out.println("Dodano ATC do historii " + atcHistory.getCallsign());
            }
        }
    }

    private void airportListUpdate() {
        for (Pilot pilot : tempListOfPilots) {
            if (pilot.getOffline_count() == 0) {
                boolean isExistArrival = false;
                boolean isExistDeparture = false;
                for (r.vatsim.view.Airport airport : tempListOfAirport) {
                    if (pilot.getAirport_departure().equals(airport.getIcao())) {
                        isExistDeparture = true;
                        airport.getDepartureList().add(pilot);
                    }

                    if (pilot.getAirport_arrival().equals(airport.getIcao())) {
                        isExistArrival = true;
                        airport.getArrivalList().add(pilot);
                    }

                    if (isExistArrival && isExistDeparture)
                        break;
                }

                //jeżeli lotnisko deprature==arrival
                if (pilot.getAirport_departure().equals(pilot.getAirport_arrival()) &&
                        !pilot.getAirport_departure().equals("") &&
                        !pilot.getAirport_arrival().equals("") &&
                        !isExistArrival && !isExistDeparture) {
                    r.vatsim.view.Airport airport = new r.vatsim.view.Airport(pilot.getAirport_departure());
                    airport.getArrivalList().add(pilot);
                    airport.getDepartureList().add(pilot);
                    tempListOfAirport.add(airport);
                } else {
                    if (!isExistArrival) {
                        r.vatsim.view.Airport airport = new r.vatsim.view.Airport(pilot.getAirport_arrival());
                        airport.getArrivalList().add(pilot);
                        tempListOfAirport.add(airport);
                    }

                    if (!isExistDeparture) {
                        r.vatsim.view.Airport airport = new r.vatsim.view.Airport(pilot.getAirport_departure());
                        airport.getDepartureList().add(pilot);
                        tempListOfAirport.add(airport);
                    }
                }
            }
        }

        //for ATC
        for (Atc atc : tempListOfAtc) {
            if (atc.getOffline_count() == 0) {
                if (atc.getAirport().equals("LIRR")) {
                    atc.setAirport("LIRF");
                    atc.setLatitude(41.8002778);
                    atc.setLongitude(12.2388889);
                }
                if (atc.getAirport().equals("LIMM")) {
                    atc.setAirport("LIMC");
                    atc.setLatitude(45.6306);
                    atc.setLongitude(8.72811);
                }

                if (atc.getAirport().equals("EDBB")) {
                    atc.setAirport("EDDT");
                    atc.setLatitude(52.5597);
                    atc.setLongitude(13.2877);
                }

                boolean isExist = false;
                for (r.vatsim.view.Airport airport : tempListOfAirport) {
                    if (!atc.getType().equals("SUP") && !atc.getType().equals("OBS") && !atc.getType().equals("FSS") && !atc.getType().equals("CTR") && !atc.getAirport().equals("")) {
                        if (airport.getIcao().equals(atc.getAirport())) {
                            isExist = true;
                            airport.getAtcList().add(atc);
                        }
                    }
                }
                if (!isExist) {
                    if (!atc.getType().equals("SUP") && !atc.getType().equals("OBS") && !atc.getType().equals("FSS") && !atc.getType().equals("CTR") && !atc.getAirport().equals("")) {
                        r.vatsim.view.Airport airport = new r.vatsim.view.Airport(atc.getAirport());
                        airport.getAtcList().add(atc);
                        tempListOfAirport.add(airport);
                    }
                }
            }
        }

        //wpisuje informacje o lotniskach
        for (r.vatsim.view.Airport airport : tempListOfAirport) {
            if (airport.getCountry() == null) {
                Airport airportDatabase = airportService.findByICAO(airport.getIcao());
                if (airportDatabase != null) {
                    if (airportDatabase.getCity() != null)
                        airport.setCity(airportDatabase.getCity());

                    if (airportDatabase.getCountry() != null)
                        airport.setCountry(airportDatabase.getCountry());

                    if (airportDatabase.getName() != null)
                        airport.setName(airportDatabase.getName());
                    airport.setElevation(airportDatabase.getElevation());
                    airport.setLatitude(airportDatabase.getLatitude());
                    airport.setLongitude(airportDatabase.getLongitude());
                    airport.setIata(airportDatabase.getIata());
                }

                if (airport.getAtcList().size() != 0) {
                    airport.setAtc(true);
                }
            }
            if (airport.getIcao().equals("")) {
                airport.setDisplay(false);
            } else {
                airport.setDisplay(true);
            }

            if (airport.getLatitude() == 0.0 && airport.getLongitude() == 0.0) {
                airport.setDisplay(false);
            }

            //sortowanie przylotów po czasie

//            if(airport.getArrivalList().size()>1){
//                airport.getArrivalList().sort(new Comparator<Pilot>() {
//                    @Override
//                    public int compare(Pilot o1, Pilot o2) {
//                        if (o1.getEstimatedTime() <= o2.getEstimatedTime()) return -1;
//                        if (o1.getEstimatedTime() > o2.getEstimatedTime()) return 1;
//                        return 0;
//                    }
//                });
//            }
        }
    }

    private void addToStats() {
        stats.setAirportSize(listOfAirport.size());
        int atcCount = listOfAtc.stream()
                .filter(item -> item.getOffline_count() == 0)
                .collect(Collectors.toList()).size();
        stats.setAtcSize(atcCount);

        int pilotCount = listOfPilots.stream()
                .filter(item -> item.getOffline_count() == 0)
                .collect(Collectors.toList()).size();

        stats.setPilotSize(pilotCount);
        stats.setTotalClient(atcCount + pilotCount);
        stats.setLastUpdate(oldLine.substring(12, 36));

    }

    //ZAPISUJE WSZYSTKIE INFORMACJE DO BAZY DANYCH (przy wylaczeniu serwera)
    public void closeSession() {
        isSessionClosed = true;
        addPilotToHistory(listOfPilots);
        listOfPilots.clear();
        listOfFlightToHistory.clear();

        addAtcToHistory(listOfAtc);
        listOfAtc.clear();
        listOfAtcToHistory.clear();
    }

    public Stats getStats() {
        return stats;
    }

    public List<r.vatsim.view.Airport> getListOfAirport() {
        return listOfAirport;
    }
}
