package r.vatsim.serviceInterface;

import org.springframework.stereotype.Service;
import r.vatsim.model.Airlines;

import java.util.List;

@Service
public interface AirlinesServiceInterface {
    Airlines add(Airlines airlines);
    List<Airlines> findAll();
    Airlines findByICAO(String icao);
}
