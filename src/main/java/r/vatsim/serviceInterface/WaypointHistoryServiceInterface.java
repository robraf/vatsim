package r.vatsim.serviceInterface;

import org.springframework.stereotype.Service;
import r.vatsim.model.WaypointHistory;

import java.util.List;

@Service
public interface WaypointHistoryServiceInterface {
    WaypointHistory add(WaypointHistory waypointHistory);
    List<WaypointHistory> findByFlightId(long id);
}
