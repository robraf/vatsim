package r.vatsim.serviceInterface;

import org.springframework.stereotype.Service;
import r.vatsim.model.FlightHistory;

import java.util.List;

@Service
public interface FlightHistoryServiceInterface {
    FlightHistory add(FlightHistory flightHistory);
    FlightHistory findById(long id);
    List<FlightHistory> findAll();
    List<FlightHistory> findByCallsign(String callsign);
    List<FlightHistory> findByCid(String cid);
}
