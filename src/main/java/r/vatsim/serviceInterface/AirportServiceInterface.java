package r.vatsim.serviceInterface;

import org.springframework.stereotype.Service;
import r.vatsim.model.Airport;

import java.util.List;

@Service
public interface AirportServiceInterface {
    Airport add(Airport airport);
    List<Airport> findAll();
    Airport findByICAO(String icao);
    List<Airport> findByCountry(String country);
}
