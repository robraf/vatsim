package r.vatsim.serviceInterface;

import org.springframework.stereotype.Service;
import r.vatsim.model.AtcHistory;

import java.util.List;

@Service
public interface AtcHistoryServiceInterface {
    AtcHistory add(AtcHistory atcHistory);
    List<AtcHistory> findByCallsign(String callsign);
    List<AtcHistory> findByCid(String cid);
    List<AtcHistory> findAll();
    List<AtcHistory> findByAirport(String airport);
}
