//package r.vatsim.model;
//
//import lombok.Data;
//
//import javax.persistence.*;
//
//@Entity
//@Data
//@Table(name = "AIRCRAFTS")
//public class Aircraft {
//    @GeneratedValue(strategy= GenerationType.IDENTITY)
//    @Id
//    @Column(name = "ID")
//    private int id;
//
//    @Column(name = "ICAO")
//    private String icao;
//
//    @Column(name = "NAME", columnDefinition="TEXT")
//    private String name;
//
//    @Column(name = "ICON")
//    private int icon;
//}
