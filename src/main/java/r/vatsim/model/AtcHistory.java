package r.vatsim.model;

import lombok.Data;

import javax.persistence.*;
import java.sql.Timestamp;

@Entity
@Data
@Table(name = "ATC_HISTORY")
public class AtcHistory {
    @GeneratedValue(strategy= GenerationType.IDENTITY)
    @Id
    @Column(name = "ID")
    private long id;

    @Column(name="CALLSIGN")
    private String callsign;

    @Column(name = "CID")
    private String cid;

    @Column(name = "AIRPORT")
    private String airport;

    @Column(name = "TIME")
    private long time;

    @Column(name = "START")
    private Timestamp start;

    @Column(name = "END")
    private Timestamp end;

    public AtcHistory(String callsign, String cid, String airport, long time, Timestamp start, Timestamp end) {
        this.callsign = callsign;
        this.cid = cid;
        this.airport = airport;
        this.time = time;
        this.start = start;
        this.end = end;
    }

    public AtcHistory(){};

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getCallsign() {
        return callsign;
    }

    public void setCallsign(String callsign) {
        this.callsign = callsign;
    }

    public String getCid() {
        return cid;
    }

    public void setCid(String cid) {
        this.cid = cid;
    }

    public String getAirport() {
        return airport;
    }

    public void setAirport(String airport) {
        this.airport = airport;
    }

    public long getTime() {
        return time;
    }

    public void setTime(long time) {
        this.time = time;
    }

    public Timestamp getStart() {
        return start;
    }

    public void setStart(Timestamp start) {
        this.start = start;
    }

    public Timestamp getEnd() {
        return end;
    }

    public void setEnd(Timestamp end) {
        this.end = end;
    }
}
