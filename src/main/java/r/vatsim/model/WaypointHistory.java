package r.vatsim.model;

import lombok.Data;

import javax.persistence.*;
import java.sql.Timestamp;


@Entity
@Data
@Table(name = "WAYPOINT_HISTORY")
public class WaypointHistory {

    @GeneratedValue(strategy= GenerationType.IDENTITY)
    @Id
    @Column(name = "ID")
    private int id;

    @Column(name = "FLIGHT_ID")
    private long flightHistory;

    @Column(name = "LATITUDE")
    private double latitude;

    @Column(name = "LONGITUDE")
    private double longitude;

    @Column(name = "SPEED")
    private int speed;

    @Column(name = "ALTITIUDE")
    private int altitiude;

    @Column(name = "HEADING")
    private int heading;

    @Column(name = "DATE")
    private Timestamp date;

    public WaypointHistory(long flightHistory, double latitude, double longitude, int speed, int altitiude, Timestamp date, int heading) {
        this.flightHistory = flightHistory;
        this.latitude = latitude;
        this.longitude = longitude;
        this.speed = speed;
        this.altitiude = altitiude;
        this.date = date;
        this.heading = heading;
    }

    public WaypointHistory(){};

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public long getFlightHistory() {
        return flightHistory;
    }

    public void setFlightHistory(long flightHistory) {
        this.flightHistory = flightHistory;
    }

    public double getLatitude() {
        return latitude;
    }

    public void setLatitude(double latitude) {
        this.latitude = latitude;
    }

    public double getLongitude() {
        return longitude;
    }

    public void setLongitude(double longitude) {
        this.longitude = longitude;
    }

    public int getSpeed() {
        return speed;
    }

    public void setSpeed(int speed) {
        this.speed = speed;
    }

    public int getAltitiude() {
        return altitiude;
    }

    public void setAltitiude(int altitiude) {
        this.altitiude = altitiude;
    }

    public Timestamp getDate() {
        return date;
    }

    public void setDate(Timestamp date) {
        this.date = date;
    }

    public int getHeading() {
        return heading;
    }

    public void setHeading(int heading) {
        this.heading = heading;
    }
}
