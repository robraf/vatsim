package r.vatsim.model;

import jdk.nashorn.internal.ir.annotations.Ignore;
import lombok.Data;
import r.vatsim.view.Waypoint;

import javax.persistence.*;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

@Entity
@Data
@Table(name = "FLIGHT_HISTORY")
public class FlightHistory {
    @GeneratedValue(strategy= GenerationType.IDENTITY)
    @Id
    @Column(name = "ID")
    private long id;

    @Column(name="CALLSIGN")
    private String callsign;

    @Column(name = "CID")
    private String cid;

    @Column(name = "DEPARTURE")
    private String departure;

    @Column(name = "ARRIVAL")
    private String arrival;

    @Column(name = "ALTERNATE")
    private String alternate;

    @Column(name = "AIRCRAFT")
    private String aircraft;

    @Column(name = "ALTITIUDE")
    private String altitiude;

    @Column(name = "RULES")
    private String rules;

    @Column(name = "DATE_START")
    private Timestamp dateStart;

    @Column(name = "DATE_END")
    private Timestamp dateEnd;

    @Transient
    private List<WaypointHistory> waypointList = new ArrayList<>();

    @Transient
    private double distance;

    public FlightHistory(String callsign, String cid, String departure, String arrival, String alternate, String aircraft, String altitiude, String rules) {
        this.callsign = callsign;
        this.cid = cid;
        this.departure = departure;
        this.arrival = arrival;
        this.alternate = alternate;
        this.aircraft = aircraft;
        this.altitiude = altitiude;
        this.rules = rules;
    }

    public FlightHistory(){};

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getCallsign() {
        return callsign;
    }

    public void setCallsign(String callsign) {
        this.callsign = callsign;
    }

    public String getCid() {
        return cid;
    }

    public void setCid(String cid) {
        this.cid = cid;
    }

    public String getDeparture() {
        return departure;
    }

    public void setDeparture(String departure) {
        this.departure = departure;
    }

    public String getArrival() {
        return arrival;
    }

    public void setArrival(String arrival) {
        this.arrival = arrival;
    }

    public String getAlternate() {
        return alternate;
    }

    public void setAlternate(String alternate) {
        this.alternate = alternate;
    }

    public String getAircraft() {
        return aircraft;
    }

    public void setAircraft(String aircraft) {
        this.aircraft = aircraft;
    }

    public String getAltitiude() {
        return altitiude;
    }

    public void setAltitiude(String altitiude) {
        this.altitiude = altitiude;
    }

    public String getRules() {
        return rules;
    }

    public void setRules(String rules) {
        this.rules = rules;
    }

    public double getDistance() {
        return distance;
    }

    public void setDistance(double distance) {
        this.distance = distance;
    }

    public List<WaypointHistory> getWaypointList() {
        return waypointList;
    }

    public void setWaypointList(List<WaypointHistory> waypointList) {
        this.waypointList = waypointList;
    }

    public Timestamp getDateStart() {
        return dateStart;
    }

    public void setDateStart(Timestamp dateStart) {
        this.dateStart = dateStart;
    }

    public Timestamp getDateEnd() {
        return dateEnd;
    }

    public void setDateEnd(Timestamp dateEnd) {
        this.dateEnd = dateEnd;
    }
}
