package r.vatsim.model;


import lombok.Data;

import javax.persistence.*;

@Entity
@Data
@Table(name = "AIRLINES")
public class Airlines {
    @GeneratedValue(strategy= GenerationType.IDENTITY)
    @Id
    @Column(name = "ID")
    private int id;

    @Column(name = "ICAO")
    private String icao;

    @Column(name = "NAME", columnDefinition="TEXT")
    private String name;

    public Airlines() {
    }

    public Airlines(String icao, String name) {
        this.icao = icao;
        this.name = name;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getIcao() {
        return icao;
    }

    public void setIcao(String icao) {
        this.icao = icao;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
