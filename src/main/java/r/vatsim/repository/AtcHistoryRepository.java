package r.vatsim.repository;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import r.vatsim.model.AtcHistory;

import java.util.List;

@Repository
public interface AtcHistoryRepository extends CrudRepository<AtcHistory, Integer> {
    @Query(value = "SELECT * FROM ATC_HISTORY u where u.CALLSIGN = :callsign", nativeQuery = true)
    List<AtcHistory> findByCallsign(String callsign);

    @Query(value = "SELECT * FROM ATC_HISTORY u where u.CID = :cid", nativeQuery = true)
    List<AtcHistory> findByCid(String cid);

    @Query(value = "SELECT * FROM ATC_HISTORY u where u.AIRPORT = :airport", nativeQuery = true)
    List<AtcHistory> findByAirport(String airport);

}