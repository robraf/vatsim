package r.vatsim.repository;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import r.vatsim.model.FlightHistory;

import java.util.List;

@Repository
public interface FlightHistoryRepository extends CrudRepository<FlightHistory, Integer> {
    @Query(value = "SELECT * FROM FLIGHT_HISTORY u where u.CALLSIGN = :callsign", nativeQuery = true)
    List<FlightHistory> findByCallsign(String callsign);

    @Query(value = "SELECT * FROM FLIGHT_HISTORY u where u.ID = :id", nativeQuery = true)
    FlightHistory findById(long id);

}
