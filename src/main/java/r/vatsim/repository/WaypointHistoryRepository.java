package r.vatsim.repository;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import r.vatsim.model.WaypointHistory;

import java.util.List;

@Repository
public interface WaypointHistoryRepository extends CrudRepository<WaypointHistory, Long> {
    @Query(value = "SELECT * FROM WAYPOINT_HISTORY u where u.FLIGHT_ID = :id ORDER BY DATE ASC", nativeQuery = true)
    List<WaypointHistory> findByFlightId(long id);
}
