package r.vatsim.repository;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import r.vatsim.model.Airport;

import java.util.List;

@Repository
public interface AirportRepository extends CrudRepository<Airport, Integer> {

    @Query(value = "SELECT * FROM AIRPORTS u where u.ICAO = :icao",  nativeQuery = true)
    Airport findByICAO(String icao);

    @Query(value = "SELECT * FROM AIRPORTS u where u.COUNTRY = :country",  nativeQuery = true)
    List<Airport> findByCountry(String country);

}
