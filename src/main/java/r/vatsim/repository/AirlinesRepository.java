package r.vatsim.repository;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import r.vatsim.model.Airlines;

@Repository
public interface AirlinesRepository extends CrudRepository<Airlines, Integer> {
    @Query(value = "SELECT * FROM AIRLINES u where u.ICAO = :icao",  nativeQuery = true)
    Airlines findByICAO(String icao);
}
