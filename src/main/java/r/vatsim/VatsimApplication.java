package r.vatsim;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.scheduling.annotation.EnableScheduling;

@EnableScheduling
@SpringBootApplication
public class VatsimApplication {

	public static void main(String[] args) {
		SpringApplication.run(VatsimApplication.class, args);
	}

}
