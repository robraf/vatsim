package r.vatsim.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import r.vatsim.model.AtcHistory;
import r.vatsim.repository.AtcHistoryRepository;
import r.vatsim.serviceInterface.AtcHistoryServiceInterface;

import java.util.List;

@Service
public class AtcHistoryService implements AtcHistoryServiceInterface {
    @Autowired
    AtcHistoryRepository atcHistoryRepository;

    @Override
    public AtcHistory add(AtcHistory atcHistory) {
        atcHistoryRepository.save(atcHistory);
        return atcHistory;
    }

    @Override
    public List<AtcHistory> findByCallsign(String callsign) {
        return atcHistoryRepository.findByCallsign(callsign);
    }

    @Override
    public List<AtcHistory> findByCid(String cid) {
        return atcHistoryRepository.findByCid(cid);
    }

    @Override
    public List<AtcHistory> findAll() {
        return (List<AtcHistory>) atcHistoryRepository.findAll();
    }

    @Override
    public List<AtcHistory> findByAirport(String airport) {
        return atcHistoryRepository.findByAirport(airport);
    }
}
