package r.vatsim.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import r.vatsim.model.Airport;
import r.vatsim.repository.AirportRepository;
import r.vatsim.serviceInterface.AirportServiceInterface;

import java.util.List;

@Service
public class AirportService implements AirportServiceInterface {
    @Autowired
    AirportRepository airportRepository;

    @Override
    public Airport add(Airport airport) {
        if(airportRepository.findByICAO(airport.getIcao())==null){
            airportRepository.save(airport);
            return airport;
        }
        return null;
    }

    @Override
    public List<Airport> findAll() {
        return (List<Airport>) airportRepository.findAll();
    }

    @Override
    public Airport findByICAO(String icao) {
        return airportRepository.findByICAO(icao);
    }

    @Override
    public List<Airport> findByCountry(String country) {
        return airportRepository.findByCountry(country);
    }
}
