package r.vatsim.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import r.vatsim.model.FlightHistory;
import r.vatsim.model.WaypointHistory;
import r.vatsim.repository.FlightHistoryRepository;
import r.vatsim.serviceInterface.FlightHistoryServiceInterface;
import r.vatsim.session.Calculate;

import java.util.ArrayList;
import java.util.List;

@Service
public class FlightHistoryService implements FlightHistoryServiceInterface {
    List<FlightHistory> flightHistoryList = new ArrayList<>();

    @Autowired
    FlightHistoryRepository flightHistoryRepository;

    @Autowired
    WaypointHistoryService waypointHistoryService;

    @Override
    public FlightHistory add(FlightHistory flightHistory) {
        flightHistoryRepository.save(flightHistory);
        return flightHistory;
    }

    @Override
    public FlightHistory findById(long id) {
        FlightHistory flightHistory = flightHistoryRepository.findById(id);
        if(flightHistory!=null){
            List<WaypointHistory> waypointHistoryList;
            waypointHistoryList = waypointHistoryService.findByFlightId(flightHistory.getId());
            flightHistory.setWaypointList(waypointHistoryList);
            return  flightHistory;
        }
        return null;
    }

    @Override
    public List<FlightHistory> findAll() {
        return null;
    }

    @Override
    public List<FlightHistory> findByCallsign(String callsign) {
        flightHistoryList = flightHistoryRepository.findByCallsign(callsign);
        List<WaypointHistory> waypointHistoryList;
        for (FlightHistory flightHistory: flightHistoryList)
        {
            waypointHistoryList = waypointHistoryService.findByFlightId(flightHistory.getId());
            flightHistory.setWaypointList(waypointHistoryList);
//            double distance = 0.0;
//            int index = 0;
//            int waypointCount = waypointHistoryList.size();
//            for (WaypointHistory waypointHistory : waypointHistoryList) {
//                if (index-3 < waypointCount) {
//                    distance += new Calculate().distance(
//                            waypointHistory.getLatitude(),
//                            waypointHistoryList.get(index + 1).getLatitude(),
//                            waypointHistory.getLongitude(),
//                            waypointHistoryList.get(index + 1).getLongitude(),
//                            waypointHistory.getAltitiude(),
//                            waypointHistoryList.get(index + 1).getAltitiude()
//                    );
//                    System.out.println("distance "+distance);
//                    index++;
//                }
//            }
//            flightHistory.setDistance(distance);
        }

        return flightHistoryList;
    }

    @Override
    public List<FlightHistory> findByCid(String cid) {
        return null;
    }
}
