package r.vatsim.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import r.vatsim.model.Airlines;
import r.vatsim.repository.AirlinesRepository;
import r.vatsim.serviceInterface.AirlinesServiceInterface;

import java.util.List;

@Service
public class AirlinesService implements AirlinesServiceInterface {
    @Autowired
    AirlinesRepository airlinesRepository;

    @Override
    public Airlines add(Airlines airlines) {
        if(airlinesRepository.findByICAO(airlines.getIcao())==null){
            airlinesRepository.save(airlines);
            return airlines;
        }
        return null;
    }

    @Override
    public List<Airlines> findAll() {
        return (List<Airlines>) airlinesRepository.findAll();
    }

    @Override
    public Airlines findByICAO(String icao) {
        return airlinesRepository.findByICAO(icao);
    }
}
