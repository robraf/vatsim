package r.vatsim.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import r.vatsim.model.WaypointHistory;
import r.vatsim.repository.WaypointHistoryRepository;
import r.vatsim.serviceInterface.WaypointHistoryServiceInterface;

import java.util.List;

@Service
public class WaypointHistoryService implements WaypointHistoryServiceInterface {
    @Autowired
    WaypointHistoryRepository waypointHistoryRepository;

    @Override
    public WaypointHistory add(WaypointHistory waypointHistory) {
        waypointHistoryRepository.save(waypointHistory);
        return waypointHistory;
    }

    @Override
    public List<WaypointHistory> findByFlightId(long id) {
        return waypointHistoryRepository.findByFlightId(id);
    }
}
