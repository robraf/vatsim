import React from "react"
import { Row, Col, Table } from 'react-bootstrap/'
import { Link } from 'react-router-dom'
import axios from 'axios'

export default class FlightHistoryForm extends React.Component {
    constructor(props) {
      super(props);
      this.state = {
          historyError: null,
          historyLoading: false,
          history: null,
    };
  
      this.handleChange = this.handleChange.bind(this);
      this.handleSubmit = this.handleSubmit.bind(this);
    }

    templateSearch(){
        return (
            <React.Fragment>
            <div className="h1 mt-3">History of flights</div>
                    <hr />
                    <form onSubmit={this.handleSubmit}>
                        <Col>
                            <Row>
                                <label>
                                    Callsign:
                                    <input type="text" name="callsign" value={this.state.callsign} onChange={this.handleChange}/>
                                </label>
                            </Row>
                            <Row>
                                <input type="submit" value="Search" />
                            </Row>
                        </Col>    
                    </form>
            </React.Fragment>   
        )
    }
  
    handleChange(event) {
      this.setState({callsign: event.target.value});
    }
  
    handleSubmit(event) {
      this.findCallsignListHistory(this.state.callsign)
      event.preventDefault();
    }

    HistoryAPI(callsign) {
        const url = `http://77.55.236.156:8080/api/history/flight/callsign?name=${callsign}&key=${process.env.REACT_APP_API_KEY}`
        this.setState({historyLoading: true})
        axios
            .get(url)
            .then(response => {
                this.setState({
                    history: response.data,
                    historyLoading: false
                })
            })
            .catch(historyError =>
                this.setState({
                    historyError,
                    historyLoading: false
                })
            )
          
    }

    findCallsignListHistory(callsign){
        if(callsign !== ""){
           this.HistoryAPI(callsign)
        }
    }
  
    render() {

    if (this.state.historyLoading) {
        if(this.state.callsign !== "")
        {
            return <div>Loading history of flight ...</div>
        }
    }

    if(this.state.history){
        if(this.state.history.length > 0)
        {
        return (
            <React.Fragment>
        <div className="h1 mt-3">History of flights - {this.state.callsign}</div>
        <hr />
        <form onSubmit={this.handleSubmit}>
            <Col>
                <Row>
                    <label>
                        Callsign:
                        <input type="text" name="callsign" value={this.state.callsign} onChange={this.handleChange}/>
                    </label>
                </Row>
                <Row>
                    <input type="submit" value="Search" />
                </Row>
            </Col>
        </form>
        <Table striped bordered hover>
                <thead>
                    <tr>                
                        <th>CID</th>
                        <th>Departure</th>
                        <th>Arrival</th>
                        <th>Aircraft</th>
                        <th>Date start</th>
                        <th>Date end</th>
                        <th></th>
                    </tr>
                </thead>
                <tbody>
                    {this.state.history.map((p, i) =>
                        <tr key={i}><td>{p.cid}</td><td>{p.departure}</td><td>{p.arrival}</td><td>{p.aircraft}</td><td>{p.dateStart}</td><td>{p.dateEnd}</td><td><Link 
                        to={{
                            pathname:'/history/flight',
                            search:`?id=${p.id}`
                            }}
                            >View</Link></td></tr>
                    )}
                </tbody>
            </Table>
         </React.Fragment>
        )
        }
    }

      return (
        <React.Fragment>
        <div className="h1 mt-3">History of flights</div>
        <hr />
        <form onSubmit={this.handleSubmit}>
            <Col>
                <Row>
                    <label>
                        Callsign:
                        <input type="text" name="callsign" value={this.state.callsign} onChange={this.handleChange}/>
                    </label>
                </Row>
                <Row>
                    <input type="submit" value="Search" />
                </Row>
            </Col>
        </form>
    </React.Fragment>
      );
    }
  }

