import React from 'react'

class Timer extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            rotation: parseInt(props.startTimeInSeconds) || 0
        };
    }

    tick() {
        this.setState(state => ({
            rotation: state.rotation + 10
        }));
    }

    componentDidMount() {
        this.interval = setInterval(() => this.tick(), 1000);
    }

    componentWillUnmount() {
        clearInterval(this.interval);
    }

    formatAngle(angle) {
        let result = angle % 360;
        return result
    }

    render() {
        return (
            <div>
                {this.formatAngle(this.state.rotation)}
            </div>
        );
    }
}

export default Timer;