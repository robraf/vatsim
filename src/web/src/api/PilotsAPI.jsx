import React, { Component } from 'react';
import { Table } from 'react-bootstrap'
import { Link } from 'react-router-dom'
import axios from 'axios'

export default class PilotsAPI extends Component {
    state = {
        error: null,
        loading: true,
        pilots: null
    };

    async fetchAPI() {
        const url = `http://77.55.236.156:8080/api/session/pilots?key=${process.env.REACT_APP_API_KEY}`;
        await fetch(url)
        .then(response => {
            if (response.ok) {
                return response.json();
            } else {
                throw new Error('Something went wrong ...');
            }
            })
        .then(
            (result) => {
                this.setState({
                    pilots: result,
                    loading: false
                });
            },
            (error) => {
                this.setState({
                    loading: false,
                    error
                });
            }
        )
    }    

    axiosAPI() {
        const url = `http://77.55.236.156:8080/api/session/pilots?key=${process.env.REACT_APP_API_KEY}`;
        axios.get(url)
        .then(response => {
            this.setState({
                pilots: response.data,
                loading: false
            });
        })
        .catch(error => this.setState({ 
            error, 
            loading: false 
        }));
    }

    componentDidMount() {
        //this.fetchAPI()
        this.axiosAPI()
    }

    render() {
        if (this.state.error) {
            return <div>Error: {this.state.error.message}</div>
        }

        if (this.state.loading) {
            return <div>Loading...</div>
        }

        if (!this.state.pilots) {
            return <div>Didn't get a pilot...</div>
        }

        return (
            <Table striped bordered hover>
                <thead>
                    <tr>
                        <th>Callsign</th>
                        <th>CID</th>
                        <th>Link</th>
                    </tr>
                </thead>
                <tbody>
                    {this.state.pilots.map((p, i) =>
                        p.offline_count === 0 && <tr key={i}><td>{p.callsign}</td><td>{p.cid}</td><td><Link to="/">Link</Link></td></tr>
                    )}
                </tbody>
            </Table>
        )
    }
}