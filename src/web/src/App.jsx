import React from 'react';
import Header from './components/Header'
import Menu from './components/Menu'
import Footer from './components/Footer'
import { Row, Col } from 'react-bootstrap'

class App extends React.Component {
    render() {
        return (
            <div>
                <Row>
                    <Col><Header /></Col>
                </Row>
                <Row className="body">
                    <Col><Menu /></Col>
                </Row>
                <Row>
                    <Col><Footer /></Col>
                </Row>
            </div>
        );
    }
}

export default App;