import React from "react"

class ATC extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            rafiki: parseInt(props.startTimeInSeconds) || 0
        };
    }

    tick() {
        this.setState(state => ({
            rafiki: state.rafiki + 1
        }));
    }

    componentDidMount() {
        this.interval = setInterval(() => this.tick(), 1000);
    }

    componentWillUnmount() {
        clearInterval(this.interval);
    }

    render() {
        return (
            <React.Fragment>
                <div className="h1 mt-3">ATC</div>
                <hr />
                rafiki: {this.state.rafiki}
                { this.state.rafiki >= 5 ? (
                    this.setState({rafiki: 0})
                ) : null }
            </React.Fragment> 
        )
    }       
}

export default ATC;