import React from "react";
import Map from './components/Map'

const Home = () => (
  <React.Fragment>
    <div className="h1 mt-3">Map</div>
    <hr />
    <Map />
  </React.Fragment>
)

export default Home;