import React from "react";
import PilotsAPI from './api/PilotsAPI'

const Pilots = () => (
    <React.Fragment>
        <div className="h1 mt-3">Pilots</div>
        <hr />
        <PilotsAPI />
    </React.Fragment>
)

export default Pilots;