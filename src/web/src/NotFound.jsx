import React from "react";

const NotFound = () => (
    <div className="mr-5 mt-3">
        Not found.
    </div>
)

export default NotFound;