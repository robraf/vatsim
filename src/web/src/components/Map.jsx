import React from "react"
import ReactMapGL, {
  Source, Layer, Marker,
  NavigationControl, FullscreenControl,
  Popup } from 'react-map-gl'
import axios from 'axios'
import { Row, Col, ProgressBar } from 'react-bootstrap/'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faPlane, faMapMarkerAlt, faPlaneArrival, faPlaneDeparture } from '@fortawesome/free-solid-svg-icons'
import taxiing from '../assets/taxiing.svg'
import boarding from '../assets/boarding.svg'

function GetCallsignOnZoom(zoom, callsign, latitude, longitude) {
  if (zoom >= 5) {
    return (
      <Marker
        key={`callsign_${callsign}`}
        latitude={latitude}
        longitude={longitude}
        offsetLeft={12}
        offsetTop={-11}
      >
        <div
          style={{
            fontFamily: 'Roboto, sans-serif',
            fontSize: '14px',
            fontWeight: 'bold',
            background: 'rgba(1,1,1,0.15)',
            padding: '1px 5px',
            color: 'brown',
            textShadow: '-1px -1px white, 1px -1px white, -1px 1px white, 1px 1px white'
          }}
        >
          {callsign}
        </div>
      </Marker>
    )
  }
}

function GetWaypoints(plane) {
  var waypointsList = []
  var latlong = []

  plane.waypointArrayList.map(wObj => {
    latlong = [wObj.longitude, wObj.latitude]
    waypointsList.push(latlong)
    return waypointsList
  })

  const waypoints = {
    type: 'FeatureCollection',
    features: [
      {
        type: 'Feature',
        geometry: {
          type: 'LineString',
          coordinates: waypointsList
        }
      }
    ]
  }

  return waypoints
}

function GetAtcTypeCircle(atc) {
  var values = []
  switch (atc.type) {
    case 'DEL':
      values.push(10)
      values.push('red')
      break
    case 'GND':
      values.push(20)
      values.push('orange')
      break
    case 'TWR':
      values.push(40)
      values.push('cyan')
      break
    case 'APP':
      values.push(100)
      values.push('blue')
      break
    default:
      values.push(0)
      values.push('white')
      break
  }

  const geojson = {
    type: 'FeatureCollection',
    features: [
      {
        type: 'Feature',
        geometry: {
          type: 'Point',
          coordinates: [atc.longitude, atc.latitude]
        }
      }
    ]
  }

  values.push(geojson)

  return values
}

function getAtcOffset(type) {
  switch (type) {
    case "GND":
      return 0.15
    case "TWR":
      return 0.3
    case "APP":
      return 0.8
    default:
      return 0.05
  }
}

function formatTime(time) {
  var hours = Math.floor(time / 60)
  hours = hours.toString().padStart(2, '0')
  var minutes = Math.floor(time) % 60
  minutes = minutes.toString().padStart(2, '0')
  return `${hours}:${minutes}h`
}

function formatDistance(distance) {
  var result = Math.round(distance * 10) / 10
  return `${result} nm`
}

function GetPilotRules(rules) {
  switch (rules) {
    case "I":
      return "IFR"
    case "V":
      return "VFR"
    default:
      return ""
  }
}

function GetFlightStatusIcon(status) {
  switch(status) {
    case "Climbing":
      return <FontAwesomeIcon icon={faPlaneDeparture} className="fa-3x"/>
    case "Taking off":
      return <FontAwesomeIcon icon={faPlaneDeparture} className="fa-3x"/>
    case "Descending":
      return <FontAwesomeIcon icon={faPlaneArrival} className="fa-3x"/>
    case "Approaching":
      return <FontAwesomeIcon icon={faPlaneArrival} className="fa-3x"/>
    case "Landing":
      return <FontAwesomeIcon icon={faPlaneArrival} className="fa-3x"/>
    case "Cruising":
      return <FontAwesomeIcon icon={faPlane} className="fa-3x"/>
    case "Taxiing":
      return <img src={taxiing} alt="" />
    case "Boarding":
      return <img src={boarding} alt="" />
    case "Arrived":
      return <img src={boarding} alt="" />
    default:
      return ""
  }
}

function pilotInfo(pilot) {
  return (
    <div className="pilotInfo">
      <Row>
        <Col>
          <div className="h2">{pilot.callsign}
            <h6>{pilot.airline !== "none" ? pilot.airline : null}</h6>
            <h6 style={{marginBottom: '10px'}}>CID: {pilot.cid}</h6></div>
        </Col>
      </Row>
      <Row>
        <Col>
          <h5 className="section">Flight status</h5>
        </Col>
      </Row>
      <Row>
        <Col className="dep_arr">
          <h4><b>{pilot.airport_departure !== "" ? pilot.airport_departure : "N/A"}</b></h4>
          <h6>{pilot.cityDeparture}</h6>
        </Col>
        <Col className="col-3" style={pilot.airline !== "none" ? { top: '132px' } : { top: '114px' }}>
          {GetFlightStatusIcon(pilot.flightStatus)}
        </Col>
        <Col className="dep_arr">
          <h4><b>{pilot.airport_arrival !== "" ? pilot.airport_arrival : "N/A"}</b></h4>
          <h6>{pilot.cityArrival}</h6>
        </Col>
      </Row>
      <div className="progressBar"><ProgressBar animated now={pilot.distanceTotal > 0 ? (100 - pilot.distanceToDestination / pilot.distanceTotal * 100) : 0} /></div>
      <Row>
        <Col>
          <p>Estimated time: <br />
            {pilot.estimatedTime >= 0 ? (
              formatTime(pilot.estimatedTime * 60)
            ) : "N/A"}
          </p><br />
          <h6>Flight time: <br />
            {pilot.flightTime >= 0 ? (
              formatTime(pilot.flightTime * 60)
            ) : "N/A"}
          </h6>
        </Col>
        <Col>
          <p>Distance to destination: <br />
            {pilot.distanceToDestination >= 0 ? (
              formatDistance(pilot.distanceToDestination)
            ) : "N/A"}
          </p>
          <h6>Distance flown: <br />
            {pilot.distanceToDestination >= 0 && pilot.distanceTotal > 0 ? (
              pilot.distanceTotal - pilot.distanceToDestination > 0 ?
                formatDistance(pilot.distanceTotal - pilot.distanceToDestination)
                : "N/A"
            ) : "N/A"}
          </h6>
        </Col>
      </Row>
      <Row>
        <Col>
          <br />Status: {pilot.flightStatus}
        </Col>
      </Row>
      <Row>
        <Col>
          <h5 className="section">More information</h5>
        </Col>
      </Row>
      <Row>
        <Col>
          <b className="h3">{pilot.aircraft}</b><br />
          <small>Nazwa samolotu</small>
        </Col>
      </Row>
      <Row>
        <Col>
          Altitude: {pilot.altitude} ft<br />
          Ground speed: {pilot.speed} kt<br />
          Heading: {pilot.heading}°<br />
          Squawk: {pilot.transponder}
        </Col>
      </Row>
      <Row>
        <Col>
          <h5 className="section">Flight plan</h5>
        </Col>
      </Row>
      <Row>
        <Col>
          <h6>Planned altitude: <br />
            {pilot.planned_altitiude !== "" ? (pilot.planned_altitiude + " ft") : null}
          </h6>
        </Col>
        <Col>
          Rules: <br />{GetPilotRules(pilot.rules)}
        </Col>
      </Row>
      <Row>
        <div style={{ padding: '10px' }}>Route: {pilot.route}</div>
      </Row>
      {/*  
                CALLSIGN
                linia lotniczna
                -------------------
                DEPARTURE - ARRIVAL
                nazwy miejscowosci
                (SPRAWDZAC OBA CZY >= 0)
                [ ESTIMATED TIME (* 60)
                DISTANCE TO DESTINATION ]
                czas lotu, przebyty dystans
                FLIGHT STATUS
                -------------------
                AIRCRAFT
                nazwa samolotu
                ALTITUDE (ft)
                SPEED (kt)
                HEADING (degree hehe)
                -------------------
                PLANNETALTITUDE - RULES                
                ROUTE
                REMARKS
                -------------------
                SQUAWK: TRANSPONDER
            */}
    </div>
  )
}

function checkZoom(zoom) {
  if (zoom > 7) {
    return true
  } else {
    return false
  }
}

export default class Map extends React.Component {
  _isMounted = false

  state = {
    pilotsError: null,
    pilotsLoading: true,
    pilots: null,
    airportsError: null,
    airportsLoading: true,
    airports: null,
    viewport: {
      latitude: 50.879,
      longitude: 4.6997,
      width: '100%',
      height: '95%',
      zoom: 2
    },
    selectedPlane: null,
    selectedAirport: null,
    showPilotInfo: false,
    showPilotPopup: false,
    showAirportPopup: false,
    hoveringPlane: null,
    hoveringAirport: null
  }

  drawMap = () => (
    <ReactMapGL
      {...this.state.viewport}
      mapStyle='mapbox://styles/mapbox/light-v8?optimize=true' //brak bledu na: streets-v11,
      mapboxApiAccessToken={process.env.REACT_APP_MAPBOX_TOKEN}
      onViewportChange={this.handleViewportChange}
      onClick={e => {
        this.setState({ showPilotInfo: null })
        this.setState({ selectedPlane: null })
      }}
    >
      <NavigationControl className='navStyle' showCompass={false} />
      <FullscreenControl className='fullscreenStyle' />
      
      {/* render atc circles on map */}
      {this.state.airports.map(airport =>
        airport.display && airport.atcList.length > 0
          ? airport.atcList.map(atc => (
            <div key={atc.callsign}>
              {atc.type !== 'ATIS' ? (
                <Source
                  id={`atcSource_${atc.callsign}`}
                  type='geojson'
                  data={GetAtcTypeCircle(atc)[2]}
                >
                  <Marker
                    latitude={atc.latitude + getAtcOffset(atc.type)}
                    longitude={atc.longitude}
                    offsetLeft={-5 * atc.callsign.length}
                  >
                    <p style={checkZoom(this.state.viewport.zoom) ? {visibility: 'visible'} : {visibility: 'hidden'}}>{atc.callsign}</p>
                  </Marker>
                  <Layer
                    id={`atcDraw_${atc.callsign}`}
                    type='circle'
                    paint={{
                      'circle-radius': {
                        stops: [
                          [0, 0],
                          [20, this.metersToPixelsAtMaxZoom(GetAtcTypeCircle(atc)[0], atc.latitude)]
                        ],
                        base: 2
                      },
                      'circle-color': GetAtcTypeCircle(atc)[1],
                      'circle-opacity': 0.15
                    }}
                  />
                </Source>                
              ) : null}
            </div>
          ))
          : null
      )}

      {/* render pilots(planes) on map */}
      {this.state.pilots.map(pilot =>
        pilot.offline_count === 0 ? (
          pilot.inAir ? (
            <div key={pilot.callsign}>
              <Marker
                latitude={pilot.latitude}
                longitude={pilot.longitude}
                offsetLeft={-10}
                offsetTop={-10}
              >
                <FontAwesomeIcon
                  icon={faPlane}
                  className='planeMarker'
                  onClick={e => {
                    e.preventDefault()
                    this.setState({ selectedPlane: pilot })
                    this.state.showPilotInfo ? pilotInfo(this.state.selectedPlane)
                      : this.setState({ showPilotInfo: !this.state.showPilotInfo })
                  }}
                  onMouseOver={e => {this.setState({ showPilotPopup: true, hoveringPlane: pilot })}}
                  onMouseLeave={e => {this.setState({ showPilotPopup: false, hoveringPlane: null })}}
                  style={{
                    transform: `rotate(${pilot.heading - 90}deg)`
                  }}
                />
              </Marker>
              {GetCallsignOnZoom(this.state.viewport.zoom, pilot.callsign, pilot.latitude, pilot.longitude)}
            </div>
          ) : (
            this.state.viewport.zoom >= 10 ? (
              <div key={pilot.callsign}>
                <Marker
                  latitude={pilot.latitude}
                  longitude={pilot.longitude}
                  offsetLeft={-10}
                  offsetTop={-10}
                >
                  <FontAwesomeIcon
                    icon={faPlane}
                    className='planeGroundMarker'
                    onClick={e => {
                      e.preventDefault()
                      this.setState({ selectedPlane: pilot })
                      this.state.showPilotInfo ? pilotInfo(this.state.selectedPlane)
                        : this.setState({ showPilotInfo: !this.state.showPilotInfo })
                    }}
                    style={{
                      transform: `rotate(${pilot.heading - 90}deg)`
                    }}
                  />
                </Marker>
                {GetCallsignOnZoom(this.state.viewport.zoom, pilot.callsign, pilot.latitude, pilot.longitude)}
              </div>
            ) : null
          )
        ) : null
      )}

      {/* render waypoints when clicked plane */}
      {this.state.selectedPlane ? (
        <Source
          id='LineData'
          type='geojson'
          data={GetWaypoints(this.state.selectedPlane)}
        >
          <Layer
            id='LineDraw'
            type='line'
            source='LineData'
            layout={{
              'line-cap': 'round',
              'line-join': 'round'
            }}
            paint={{
              'line-color': 'green',
              'line-width': 4
            }}
          />
        </Source>
      ) : null}

      {/* render airports on map */}
      {this.state.airports.map(airport =>
        airport.display && (
            airport.atcList.length > 0 
            || (airport.arrivalList.length + airport.departureList.length) >= 6
            || this.state.viewport.zoom >= 4) ? (
          <div key={airport.icao}>
            <Marker
              latitude={airport.latitude}
              longitude={airport.longitude}
              offsetLeft={-6}
              offsetTop={-15}
              style={{ top: '50%', left: '50%', transform: 'translate(-50%, -50%)' }}
            >
              <FontAwesomeIcon
                icon={faMapMarkerAlt}
                className='airportMarker'
                onClick={e => {
                  e.preventDefault()
                  //okienko z informacjami
                }}
                onMouseOver={e => {this.setState({ showAirportPopup: true, hoveringAirport: airport })}}
                onMouseLeave={e => {this.setState({ showAirportPopup: false, hoveringAirport: null })}}
              />
            </Marker>
          </div>
        ) : null
      )}

      {/* render plane popup */}
      {this.state.showPilotPopup && this.state.viewport.zoom < 5 ? (
        <Popup
          tipSize={7}
          offsetTop={-5}
          latitude={this.state.hoveringPlane.latitude}
          longitude={this.state.hoveringPlane.longitude}
          closeButton={false}
          anchor="bottom" >
          {this.state.hoveringPlane.callsign}
        </Popup>
      ) : null}

      {/* render airport popup */}
      {this.state.showAirportPopup ? (
        <Popup
          tipSize={7}
          offsetTop={-5}
          latitude={this.state.hoveringAirport.latitude}
          longitude={this.state.hoveringAirport.longitude}
          closeButton={false}
          anchor="bottom" >
          <div>
            <b>{this.state.hoveringAirport.icao} - {this.state.hoveringAirport.name}</b><br />
            Departures: {this.state.hoveringAirport.departureList.length}<br />
            Arrivals:  {this.state.hoveringAirport.arrivalList.length}
          </div>
        </Popup>
      ) : null}

      {/* render pilot info window */}
      {this.state.showPilotInfo ? (
        pilotInfo(this.state.selectedPlane)
      ) : null}

      {/* POLYGON
            <Source id="alaskajson" type="geojson"
                data="https://raw.githubusercontent.com/glynnbird/usstatesgeojson/master/alaska.geojson">
                <Layer id="alaska" type="fill" source="alaskajson"
                    paint={{
                        "fill-color": "red",
                        "fill-opacity": 0.4
                    }}
                />
            </Source> */}
    </ReactMapGL>
  )

  async airportsAPI() {
    const url = `http://77.55.236.156:8080/api/session/airport?key=${process.env.REACT_APP_API_KEY}`
    await axios
      .get(url)
      .then(response => {
        if (this._isMounted) {
          this.setState({
            airports: response.data,
            airportsLoading: false
          })
        }
      })
      .catch(airportsError =>
        this.setState({
          airportsError,
          airportsLoading: false
        })
      )
  }

  async pilotsAPI() {
    const url = `http://77.55.236.156:8080/api/session/pilots?key=${process.env.REACT_APP_API_KEY}`
    await axios
      .get(url)
      .then(response => {
        if (this._isMounted) {
          this.setState({
            pilots: response.data,
            pilotsLoading: false
          })
        }
      })
      .catch(pilotsError =>
        this.setState({
          pilotsError,
          pilotsLoading: false
        })
      )
  }

  componentDidMount() {
    this._isMounted = true

    window.addEventListener("resize", this.resize);
    this.resize();

    //Get Airports from API
    this.airportsAPI()

    //Get Pilots from API
    this.pilotsAPI()
  }

  componentWillUnmount() {
    this._isMounted = false

    window.removeEventListener("resize", this.resize);
  }

  resize = () => {
    this.handleViewportChange({
      width: window.innerWidth * 0.82,
      height: window.innerHeight * 0.75
    });
  };

  handleViewportChange = viewport => {
    this.setState({
      viewport: { ...this.state.viewport, ...viewport }
    });
  };

  metersToPixelsAtMaxZoom = (meters, latitude) =>
    (meters * 1000) / 0.075 / Math.cos((latitude * Math.PI) / 180)

  render() {
    //Check if airports loaded correctly
    if (this.state.airportsError) {
      return <div>Error: {this.state.airportsError.message}</div>
    }

    if (this.state.airportsLoading) {
      return <div>Loading airports...</div>
    }

    if (!this.state.airports) {
      return <div>Didn't get an airport...</div>
    }

    //Check if pilots loaded correctly
    if (this.state.pilotsError) {
      return <div>Error: {this.state.pilotsError.message}</div>
    }

    if (this.state.pilotsLoading) {
      return <div>Loading pilots...</div>
    }

    if (!this.state.pilots) {
      return <div>Didn't get a pilot...</div>
    }

    return (
      <React.Fragment>
        {this.drawMap()}
      </React.Fragment>
    )
  }
}