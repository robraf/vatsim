import React from "react";
import { Nav, Row, Col } from 'react-bootstrap/'
import { Switch, Route, Link } from 'react-router-dom';
import Home from '../Home'
import History from '../History'
import HistoryDetail from "../HistoryDetail";
import Pilots from '../Pilots'
import ATC from '../ATC'
import About from '../About'
import NotFound from '../NotFound'
import Mapeczka from './Mapp'

const Menu = () => (
  <React.Fragment>
    <Row>
      <Col className="col-2 menu">
        <Nav className="flex-column">
          <Nav.Link href="/" as={Link} to="/">Home</Nav.Link>
          <Nav.Link eventKey="history" as={Link} to="/history">Flight history</Nav.Link>
          <Nav.Link eventKey="pilots" as={Link} to="/pilots">Pilots</Nav.Link>
          <Nav.Link eventKey="atc" as={Link} to="/atc">ATC</Nav.Link>
          <Nav.Link eventKey="about" as={Link} to="/about">About</Nav.Link>
          <Nav.Link eventKey="testmap" as={Link} to="/testmap">Test Map</Nav.Link>
          <hr />
          <Nav.Link eventKey="report" as={Link} to="/report">Report a bug</Nav.Link>
        </Nav>
      </Col>
      <Col className="col-10 content">
        <Switch>
          <Route exact path='/history' component={History} />
          <Route exact path='/history/flight' component={HistoryDetail} />
          <Route exact path='/pilots' component={Pilots} />
          <Route exact path='/atc' component={ATC} />
          <Route exact path='/about' component={About} />
          <Route exact path='/testmap' component={Mapeczka} />
          <Route exact path='/' component={Home} />
          <Route component={NotFound} />
        </Switch>
      </Col>
    </Row>
  </React.Fragment>
)

export default Menu;