import React from "react"
import ReactMapboxGL, { Layer, Feature, Popup, GeoJSONLayer, ZoomControl } from 'react-mapbox-gl'
import { Row, Col, ProgressBar } from 'react-bootstrap/'
import axios from 'axios'
import planeIcon from '../assets/plane.svg'
import airportIcon from '../assets/airport.svg'
import boardingIcon from '../assets/boarding.svg'
import climbingIcon from '../assets/climbing.svg'
import descendingIcon from '../assets/descending.svg'
import taxiingIcon from '../assets/taxiing.svg'

const Map = ReactMapboxGL({ accessToken: process.env.REACT_APP_MAPBOX_TOKEN })

function formatTime(time) {
    var hours = Math.floor(time / 60)
    hours = hours.toString().padStart(2, '0')
    var minutes = Math.floor(time) % 60
    minutes = minutes.toString().padStart(2, '0')
    return `${hours}:${minutes}h`
}

function formatDistance(distance) {
    var result = Math.round(distance * 10) / 10
    return `${result} nm`
}

function pilotsToShow(pilots) {
    var tab1 = []
    var tab2 = []

    pilots.map(pilot => (
        pilot.offline_count === 0 ? (
            pilot.inAir ? (
                tab1.push(pilot)
            ) : (
                tab2.push(pilot)
            )
        ) : null
    ))

    var tab = [tab1, tab2]
    return tab
}

function airportsToShow(airports) {
    var tab = []

    airports.map(airport => (
        airport.display && (
            airport.atcList.length > 0
            || (airport.arrivalList.length + airport.departureList.length) >= 6) ? (
                tab.push(airport)
            ) : null
    ))

    return tab
}

function atcToShow(airports) {
    var tab = []

    airports.map(airport => (
        airport.display && airport.atcList.length > 0) ? (
            airport.atcList.map(atc => (
                atc.type !== 'ATIS' ? (
                    tab.push(atc)
                ) : null
            ))
        ) : null
    )

    return tab
}

function GetPilotRules(rules) {
    switch (rules) {
        case "I":
            return "IFR"
        case "V":
            return "VFR"
        default:
            return ""
    }
}

function GetFlightStatusIcon(status) {
    switch (status) {
        case "Climbing":
            return <img src={climbingIcon} alt="" />
        case "Taking off":
            return <img src={climbingIcon} alt="" />
        case "Descending":
            return <img src={descendingIcon} alt="" />
        case "Approaching":
            return <img src={descendingIcon} alt="" />
        case "Landing":
            return <img src={descendingIcon} alt="" />
        case "Cruising":
            return <img src={planeIcon} alt="" />
        case "Taxiing":
            return <img src={taxiingIcon} alt="" />
        case "Boarding":
            return <img src={boardingIcon} alt="" />
        case "Arrived":
            return <img src={boardingIcon} alt="" />
        default:
            return ""
    }
}

function pilotInfo(pilot) {
    return (
        <div className="pilotInfo">
            <Row>
                <Col>
                    <div className="h2">{pilot.callsign}
                        <h6>{pilot.airline !== "none" ? pilot.airline : null}</h6>
                        <h6 style={{ marginBottom: '10px' }}>CID: {pilot.cid}</h6></div>
                </Col>
            </Row>
            <Row>
                <Col>
                    <h5 className="section">Flight status</h5>
                </Col>
            </Row>
            <Row>
                <Col className="dep_arr">
                    <h4><b>{pilot.airport_departure !== "" ? pilot.airport_departure : "N/A"}</b></h4>
                    <h6>{pilot.cityDeparture}</h6>
                </Col>
                <Col className="col-3" style={pilot.airline !== "none" ? { top: '132px' } : { top: '114px' }}>
                    {GetFlightStatusIcon(pilot.flightStatus)}
                </Col>
                <Col className="dep_arr">
                    <h4><b>{pilot.airport_arrival !== "" ? pilot.airport_arrival : "N/A"}</b></h4>
                    <h6>{pilot.cityArrival}</h6>
                </Col>
            </Row>
            <div className="progressBar"><ProgressBar animated now={pilot.distanceTotal > 0 ? (100 - pilot.distanceToDestination / pilot.distanceTotal * 100) : 0} /></div>
            <Row>
                <Col>
                    <p>Estimated time: <br />
                        {pilot.estimatedTime >= 0 ? (
                            formatTime(pilot.estimatedTime * 60)
                        ) : "N/A"}
                    </p><br />
                    <h6>Flight time: <br />
                        {pilot.flightTime >= 0 ? (
                            formatTime(pilot.flightTime * 60)
                        ) : "N/A"}
                    </h6>
                </Col>
                <Col>
                    <p>Distance to destination: <br />
                        {pilot.distanceToDestination >= 0 ? (
                            formatDistance(pilot.distanceToDestination)
                        ) : "N/A"}
                    </p>
                    <h6>Distance flown: <br />
                        {pilot.distanceToDestination >= 0 && pilot.distanceTotal > 0 ? (
                            pilot.distanceTotal - pilot.distanceToDestination > 0 ?
                                formatDistance(pilot.distanceTotal - pilot.distanceToDestination)
                                : "N/A"
                        ) : "N/A"}
                    </h6>
                </Col>
            </Row>
            <Row>
                <Col>
                    <br />Status: {pilot.flightStatus}
                </Col>
            </Row>
            <Row>
                <Col>
                    <h5 className="section">More information</h5>
                </Col>
            </Row>
            <Row>
                <Col>
                    <b className="h3">{pilot.aircraft}</b><br />
                    <small>Nazwa samolotu</small>
                </Col>
            </Row>
            <Row>
                <Col>
                    Altitude: {pilot.altitude} ft<br />
            Ground speed: {pilot.speed} kt<br />
            Heading: {pilot.heading}°<br />
            Squawk: {pilot.transponder}
                </Col>
            </Row>
            <Row>
                <Col>
                    <h5 className="section">Flight plan</h5>
                </Col>
            </Row>
            <Row>
                <Col>
                    <h6>Planned altitude: <br />
                        {pilot.planned_altitiude !== "" ? (pilot.planned_altitiude + " ft") : null}
                    </h6>
                </Col>
                <Col>
                    Rules: <br />{GetPilotRules(pilot.rules)}
                </Col>
            </Row>
            <Row>
                <div style={{ padding: '10px' }}>Route: {pilot.route}</div>
            </Row>
            {/*  
                  CALLSIGN
                  linia lotniczna
                  -------------------
                  DEPARTURE - ARRIVAL
                  nazwy miejscowosci
                  (SPRAWDZAC OBA CZY >= 0)
                  [ ESTIMATED TIME (* 60)
                  DISTANCE TO DESTINATION ]
                  czas lotu, przebyty dystans
                  FLIGHT STATUS
                  -------------------
                  AIRCRAFT
                  nazwa samolotu
                  ALTITUDE (ft)
                  SPEED (kt)
                  HEADING (degree hehe)
                  -------------------
                  PLANNETALTITUDE - RULES                
                  ROUTE
                  REMARKS
                  -------------------
                  SQUAWK: TRANSPONDER
              */}
        </div>
    )
}

function GetWaypoints(waypoints, callsign) {
    var waypointsList = []
    var latlong = []

    waypoints.map(wObj => (
        wObj.callsign === callsign ? (
            wObj.waypointList.map(wp => {
                latlong = [wp.longitude, wp.latitude]
                waypointsList.push(latlong)
            })
        ) : null
    ))

    const result = {
        type: 'FeatureCollection',
        features: [
            {
                type: 'Feature',
                geometry: {
                    type: 'LineString',
                    coordinates: waypointsList
                }
            }
        ]
    }

    return result
}

function GetAtcTypeCircle(atc) {
    var values = []
    switch (atc.type) {
        case 'DEL':
            values.push(10)
            values.push('red')
            break
        case 'GND':
            values.push(20)
            values.push('orange')
            break
        case 'TWR':
            values.push(40)
            values.push('cyan')
            break
        case 'APP':
            values.push(100)
            values.push('blue')
            break
        default:
            values.push(0)
            values.push('white')
            break
    }

    const geojson = {
        type: 'FeatureCollection',
        features: [
            {
                type: 'Feature',
                geometry: {
                    type: 'Point',
                    coordinates: [atc.longitude, atc.latitude]
                }
            }
        ]
    }

    values.push(geojson)

    return values
}

export default class Mapp extends React.Component {
    state = {
        pilotsError: null,
        pilotsLoading: true,
        pilots: null,
        airportsError: null,
        airportsLoading: true,
        airports: null,
        waypointsError: null,
        waypointsLoading: true,
        waypoints: null,
        viewport: {
            latitude: 50.879,
            longitude: 4.6997,
            width: '100%',
            height: '95%',
            zoom: [2]
        },
        currentMapZoom: 1,
        selectedPlane: null,
        selectedAirport: null,
        showPilotInfo: false,
        showPilotPopup: false,
        showAirportPopup: false,
        hoveringPlane: null,
        hoveringAirport: null,
    }

    pilotsAPI() {
        const url = `http://77.55.236.156:8080/api/session/pilots?key=${process.env.REACT_APP_API_KEY}`
        axios
            .get(url, {
                headers: {
                    'Content-Type': 'application/json',
                    'Accept': 'application/json'
                }
            })
            .then(response => {
                this.setState({
                    pilots: response.data,
                    pilotsLoading: false
                })
            })
            .catch(pilotsError =>
                this.setState({
                    pilotsError,
                    pilotsLoading: false
                })
            )
    }

    airportsAPI() {
        const url = `http://77.55.236.156:8080/api/session/airport?key=${process.env.REACT_APP_API_KEY}`
        axios
            .get(url, {
                headers: {
                    'Content-Type': 'application/json',
                    'Accept': 'application/json'
                }
            })
            .then(response => {
                this.setState({
                    airports: response.data,
                    airportsLoading: false
                })
            })
            .catch(airportsError =>
                this.setState({
                    airportsError,
                    airportsLoading: false
                })
            )
    }

    waypointsAPI() {
        const url = `http://77.55.236.156:8080/api/session/pilots/waypoints?key=${process.env.REACT_APP_API_KEY}`
        axios
            .get(url, {
                headers: {
                    'Content-Type': 'application/json',
                    'Accept': 'application/json'
                }
            })
            .then(response => {
                this.setState({
                    waypoints: response.data,
                    waypointsLoading: false
                })
            })
            .catch(waypointsError =>
                this.setState({
                    waypointsError,
                    waypointsLoading: false
                })
            )
    }

    componentDidMount() {
        window.addEventListener("resize", this.resize);
        this.resize();

        //Get Airports from API
        this.airportsAPI()

        //Get Pilots from API
        this.pilotsAPI()

        //Get Waypoints from API
        this.waypointsAPI()
    }

    componentWillUnmount() {
        window.removeEventListener("resize", this.resize);
    }

    resize = () => {
        this.handleViewportChange({
            width: window.innerWidth * 0.82,
            height: window.innerHeight * 0.75
        })
    }

    handleViewportChange = viewport => {
        this.setState({
            viewport: { ...this.state.viewport, ...viewport }
        })
    }

    render() {
        const plane = new Image(20, 20)
        plane.src = planeIcon
        const plane_ground = new Image(20,20)
        plane_ground.src = planeIcon
        const airfield = new Image(10, 12)        
        airfield.src = airportIcon

        //Check if airports loaded correctly
        if (this.state.airportsError) {
            return <div>Error: {this.state.airportsError.message}</div>
        }

        if (this.state.airportsLoading) {
            return <div>Loading airports...</div>
        }

        if (!this.state.airports) {
            return <div>Didn't get an airport...</div>
        }

        //Check if pilots loaded correctly
        if (this.state.pilotsError) {
            return <div>Error: {this.state.pilotsError.message}</div>
        }

        if (this.state.pilotsLoading) {
            return <div>Loading pilots...</div>
        }

        if (!this.state.pilots) {
            return <div>Didn't get a pilot...</div>
        }

        //Check if pilots loaded correctly
        if (this.state.waypointsError) {
            return <div>Error: {this.state.waypointsError.message}</div>
        }

        if (this.state.waypointsLoading) {
            return <div>Loading waypoints...</div>
        }

        if (!this.state.waypoints) {
            return <div>Didn't get waypoints...</div>
        }

        return (
            <div style={{ marginTop: '50px' }}>
                <Map {...this.state.viewport}
                    onStyleLoad={el => {
                        this.map = el
                        this.map.dragRotate.disable()
                        this.map.touchZoomRotate.disableRotation()
                        this.map.addImage("plane", plane)
                        this.map.addImage("airfield", airfield)
                    }}
                    style="mapbox://styles/mapbox/light-v8"
                    containerStyle={{
                        height: '80vh',
                        width: '100%'
                    }}
                    onClick={e => {
                        this.setState({ showPilotInfo: null })
                        this.setState({ selectedPlane: null })
                    }}
                    onZoomStart={e => {
                        this.setState({ currentMapZoom: this.map.getZoom()})
                    }}
                >
                    {/* render atc circles on map */}
                    {atcToShow(this.state.airports).map((atc, i) => (
                        <GeoJSONLayer
                            key={i}
                            data={GetAtcTypeCircle(atc)[2]}
                            circlePaint={{
                                'circle-radius': {
                                    stops: [
                                        [0, 0],
                                        [20, GetAtcTypeCircle(atc)[0] * 13333]
                                    ],
                                    base: 2
                                },
                                'circle-color': GetAtcTypeCircle(atc)[1],
                                'circle-opacity': 0.15
                            }}
                        />
                    ))}

                    {/* render pilots in air on map */}
                    <Layer type="symbol"
                        id="planes"
                        layout={{ 'icon-image': 'plane', "icon-allow-overlap": true }}                        
                    >
                        {pilotsToShow(this.state.pilots)[0].map((pilot, i) => (
                            <Feature key={i}
                                coordinates={[pilot.longitude, pilot.latitude]}
                                onClick={e => {
                                    this.setState({ selectedPlane: pilot })
                                    this.state.showPilotInfo ? pilotInfo(this.state.selectedPlane)
                                        : this.setState({ showPilotInfo: !this.state.showPilotInfo })
                                }}
                                onMouseEnter={e => {
                                    this.setState({ showPilotPopup: true, hoveringPlane: pilot })
                                }}
                                onMouseLeave={e => {
                                    this.setState({ showPilotPopup: false, hoveringPlane: null })
                                }}                                
                            />
                        ))}
                    </Layer>

                    {/* render pilots on ground on map */}
                    {this.state.currentMapZoom >= 10 ? (
                        <Layer type="symbol"
                            id="planes_ground"
                            layout={{ 'icon-image': 'plane', "icon-allow-overlap": true }}
                        >
                            {pilotsToShow(this.state.pilots)[1].map((pilot, j) => (
                                <Feature key={j}
                                    coordinates={[pilot.longitude, pilot.latitude]}
                                    onClick={e => {
                                        this.setState({ selectedPlane: pilot })
                                        this.state.showPilotInfo ? pilotInfo(this.state.selectedPlane)
                                            : this.setState({ showPilotInfo: !this.state.showPilotInfo })
                                    }}
                                    onMouseEnter={e => {
                                        this.setState({ showPilotPopup: true, hoveringPlane: pilot })
                                    }}
                                    onMouseLeave={e => {
                                        this.setState({ showPilotPopup: false, hoveringPlane: null })
                                    }}
                                />
                            ))}
                        </Layer>
                    ) : null}

                    {/* render airports on map */}
                    <Layer type="symbol"
                        id="airports"
                        layout={{ 'icon-image': 'airfield', "icon-allow-overlap": true }}
                    >
                        {airportsToShow(this.state.airports, this.state.currentMapZoom).map((airport, i) => (
                            <Feature key={i}
                                coordinates={[airport.longitude, airport.latitude]}
                                onMouseEnter={e => {
                                    this.setState({ showAirportPopup: true, hoveringAirport: airport })
                                }}
                                onMouseLeave={e => {
                                    this.setState({ showAirportPopup: false, hoveringAirport: null })
                                }}
                            />
                        ))}
                    </Layer>

                    {/* render waypoints when clicked plane */}
                    {this.state.selectedPlane ? (
                        <GeoJSONLayer
                            data={GetWaypoints(this.state.waypoints, this.state.selectedPlane.callsign)}
                            linePaint={{
                                'line-color': 'green',
                                'line-width': 4
                            }}
                            lineLayout={{
                                'line-cap': 'round',
                                'line-join': 'round'
                            }}
                        />
                    ) : null}

                    {/* render plane popup */}
                    {this.state.showPilotPopup && this.state.viewport.zoom < 5 ? (
                        <Popup
                            coordinates={[this.state.hoveringPlane.longitude, this.state.hoveringPlane.latitude]}
                            closeButton={false}
                            anchor="bottom"
                        >
                            {this.state.hoveringPlane.callsign}
                        </Popup>
                    ) : null}

                    {/* render airport popup */}
                    {this.state.showAirportPopup ? (
                        <Popup
                            coordinates={[this.state.hoveringAirport.longitude, this.state.hoveringAirport.latitude]}
                            closeButton={false}
                            anchor="bottom"
                        >
                            <div>
                                <b>{this.state.hoveringAirport.icao} - {this.state.hoveringAirport.name}</b><br />
                                Departures: {this.state.hoveringAirport.departureList.length}<br />
                                Arrivals:  {this.state.hoveringAirport.arrivalList.length}
                            </div>
                        </Popup>
                    ) : null}

                    {/* render pilot info window */}
                    {this.state.showPilotInfo ? (
                        pilotInfo(this.state.selectedPlane)
                    ) : null}

                    <ZoomControl />
                </Map>
            </div>
        )
    }
}