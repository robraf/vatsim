import React from 'react';
import logo from '../assets/logo192.png'
import { Navbar, Nav } from 'react-bootstrap/'
import { Link } from 'react-router-dom';

const Header = () => (
    <React.Fragment>
        <Navbar bg="dark" variant="dark">
            <Navbar.Brand href="/" as={Link} to="/" className="ml-5">
                <img alt="Logo" src={logo} />{' '}
                VATSIM
            </Navbar.Brand>
            <Navbar.Toggle aria-controls="responsive-navbar-nav" />
            <Nav className="ml-auto pr-4">
                <Nav.Link href="/login" as={Link} to="/login" className="login">Login</Nav.Link>
            </Nav>
        </Navbar>
    </React.Fragment>
);

export default Header;