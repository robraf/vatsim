import React from 'react';

const Footer = () => (
    <footer className="py-1 bg-dark text-center">
        <small className="text-white">
            Copyright 2020 © 2RStudio
        </small>
    </footer>
)

export default Footer;