import React from "react"
import queryString from 'query-string';
import axios from 'axios'

export default class HistoryDetail extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            historyError: null,
            historyLoading: false,
            history: null,
      };
      }
    
      componentWillMount() {
        this.getFlightInfo();
      }

    getFlightInfo(){
        let params = queryString.parse(this.props.location.search)
        const url = `http://77.55.236.156:8080/api/history/flight/id?name=${params.id}&key=${process.env.REACT_APP_API_KEY}`
        this.setState({historyLoading: true})
        axios
            .get(url)
            .then(response => {
                this.setState({
                    history: response.data,
                    historyLoading: false
                })
            })
            .catch(historyError =>
                this.setState({
                    historyError,
                    historyLoading: false
                })
            )
    }


    render() {
      
        if (this.state.historyLoading) {
                return <div>Loading history of flight ...</div>
        }

        if (this.state.historyError) {
            return <div>Something went wrong...</div>
        }

        if(this.state.history){
            return (
            <div>
                <b>History of flight {this.state.history.callsign}</b><br/>
                From: {this.state.history.departure}<br/>
                To: {this.state.history.arrival}<br/>
            </div>)
        }

        return <div>This flight not exist</div>
    }
}