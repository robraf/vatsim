import React from 'react';
import './css/style.css'

const About = () => (
    <React.Fragment>
        <div className="h1 mt-3">About us</div>
        <hr />
        <div>
            Lorem ipsum dolor sit amet, consectetur adipiscing elit. In ac orci id enim varius auctor ornare id odio. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. 
            Mauris nec elementum elit, hendrerit tincidunt metus. Etiam fringilla, odio sit amet sagittis porttitor, nulla nunc dignissim lectus, a commodo nulla diam quis arcu. Phasellus tristique hendrerit felis, 
            a ornare nisl sodales nec. Vivamus tempus finibus purus, ut imperdiet quam commodo id. Donec aliquet, dolor nec lobortis sollicitudin, turpis eros luctus magna, eget semper justo enim non risus.
        
            Proin sed tincidunt quam, sit amet congue odio. Phasellus dapibus diam ac turpis luctus consectetur. Donec gravida maximus neque, in aliquam arcu cursus a. Suspendisse eu felis ac ex placerat malesuada. 
            tincidunt sapien nibh, non pellentesque est fringilla eget. Fusce in ipsum sit amet erat pharetra placerat. Maecenas at orci ac neque consequat cursus. Nulla at volutpat orci. Pellentesque habitant morbi 
            tristique senectus et netus et malesuada fames ac turpis egestas. Quisque non ullamcorper quam. Quisque efficitur lobortis orci eget elementum. Mauris eleifend, ante blandit auctor efficitur, ex enim 
            fringilla nisi, ut eleifend ex tellus vitae tellus. Duis tincidunt neque magna, at viverra orci ultricies quis. Quisque dapibus dui nec accumsan tincidunt.
        </div>
    </React.Fragment>
)

export default About;